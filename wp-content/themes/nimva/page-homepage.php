<?php // Template Name: Home Page ?>
<?php get_header(); ?>

<div class="container-fluid clearfix">
    <div class="row">


    <div class="slider-wrap">


	    <?php

	    $images = get_field('homepage_gallery');

	    if( $images ): ?>
            <div id="slider" class="flexslider">
                <ul class="slides">
				    <?php foreach( $images as $image ): ?>
                        <li>
                            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                            <p><?php echo $image['caption']; ?></p>
                        </li>
				    <?php endforeach; ?>
                </ul>
            </div>

	    <?php endif; ?>


        <script>
            $(window).load(function() {
                $('.flexslider').flexslider({
                    animation: "slide"
                });
            });
        </script>



    </div>
</div>

    <section class="home-main-section">
        <div class="container">
            <h1><?php the_field('main_headline'); ?></h1>
            <div class="content">
                <?php the_field('main_content'); ?>
            </div>
        </div>
    </section>
    </div>
<?php get_footer(); ?>