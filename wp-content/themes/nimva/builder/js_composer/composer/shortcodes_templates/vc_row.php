<?php
$output = $el_class = '';
extract(shortcode_atts(array(
	'layout' => 'normal',
	'margintop' => '',
	'marginbottom' => '',
	'paddingtop' => '30',
	'paddingbottom' => '30',
	'bg_color' => '',
	'border_color' => '',
	'border_width' => '',
	'bg_image' => '',
	'bg_patterns' => '',
	'bg_attachment' => 'scroll',
	'bg_repeat' => 'repeat',
	'bg_stretch' => 'false',
	'bg_parallax' => 'false',
	'video_bg' =>'false',
	'ytb_video_id' => '',
	'vid_opacity' => '70',
	'video_overlay' => '',
	'video_overlay_col' =>  '',
	'video_overlay_opac' => '',
	'row_font_color' => '',
	'video_quality' => '',
    'el_class' => '',
), $atts));

wp_enqueue_style( 'js_composer_front' );
wp_enqueue_script( 'wpb_composer_front_js' );
wp_enqueue_style('js_composer_custom_css');

$el_class = $this->getExtraClass($el_class);

$css_class =  apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'wpb_row '.get_row_css_class().$el_class, $this->settings['base']);

if ( $paddingtop != '0' ) $paddingt='padding-top:'.$paddingtop.'px; '; else $paddingt = '';
if ( $paddingbottom != '0' ) $paddingb='padding-bottom:'.$paddingbottom.'px; '; else $paddingb = '';

if ( $margintop != '0' ) $margint='margin-top:'.$margintop.'px; '; else $margint = '';
if ( $marginbottom != '0' ) $marginb='margin-bottom:'.$marginbottom.'px; '; else $marginb = '';

if( $bg_color ) $bg = 'background-color:'.$bg_color.';';  else $bg = '';

$border='';
if ($border_width > '0') $border = 'border: ' . $border_width . 'px solid ' . $border_color . '; border-left: 0; border-right: 0;';

$image = '';

//if we have a background attached let's display it
if ( $bg_image ) { 
	$img = wp_get_attachment_image_src($bg_image, 'full');
	$image = 'background: url('.$img[0].')'. ' ' .$bg_repeat.'; background-attachment:'.$bg_attachment.';'; 
}

//if no background image is attached but we have a pattern selected
if( !$bg_image && $bg_patterns) {

	$image = 'background: url('.get_template_directory_uri().'/images/patterns/'.$bg_patterns.')'. ' ' .$bg_repeat.'; background-attachment:'.$bg_attachment.';';
}

if( $bg_stretch == 'true' ) $bg_stretch = ' bg_cover_size'; else $bg_stretch ='';

if( $bg_parallax == 'true' ) { $bg_parallax = ' parallax_section '; $data_type='data-type="background"'; } else { $bg_parallax =''; $data_type=''; }

if( $row_font_color ) $font = 'color:'.$row_font_color.';'; else $font ='';

if ($video_bg == 'true' && $ytb_video_id !='') {
	
	$video_quality = 'default';
	
	$vid_opacity = $vid_opacity / 100;	
	
	$video_overlay_opac = $video_overlay_opac / 100;
		
	$opacity = 'opacity: ' . $video_overlay_opac . '; filter: alpha(opacity=' . $video_overlay_opac . ')';
	
	$output .= '<div class="video_bg_row front '.$ytb_video_id.' '. $css_class  . '" style="'.$margint.$marginb.$font.$paddingt.$paddingb.$border.'">'; 
	           
		$output .= '<div id="'.$ytb_video_id.'" class="player video-container" data-id="'.$ytb_video_id.'" data-property="{videoURL:\''.$ytb_video_id.'\',containment: \'.'.$ytb_video_id.'\' ,autoPlay:true, showControls:false, mute:true, loop:true, startAt:0,quality:\''.$video_quality.'\', opacity:'.$vid_opacity.'}" data-opacity="'.$vid_opacity.'"></div>';
		
		//$output .= '<div id="P1" class="player video-container" style="background: rgba(0,0,0,0.2)" data-property="{videoURL:\'http://youtu.be/l_tHTmd5pgk\',containment:\'#P1\',startAt:0,mute:true,autoPlay:true,loop:true,opacity:1}"></div>';
		
		$video_overlay = ( $video_overlay !='' ) ? 'background-image: url( ' .get_template_directory_uri().'/images/overlay/'. $video_overlay . ' );' : '';
		
		$video_overlay_col = ($video_overlay_col != '' ) ? 'background-color: ' . $video_overlay_col . ';'  : '' ;
		
		$output .= '<div class="video_overlay" style="' . $video_overlay . $video_overlay_col . $opacity . '"></div>';
}

else {
	$output .= '<div class="'. $bg_parallax . $css_class  . $bg_stretch . '" '.$data_type.' data-speed="4" style="'.$margint.$marginb.$bg.$image.$font.$paddingt.$paddingb.$border.'">';
}
$class = '';
if($layout == 'wide'): $class = 'full_width'; endif;
$output .= '<div class="container '.$class.' clearfix">';
$output .= wpb_js_remove_wpautop($content);
$output .= '</div>';
$output .= '</div>'.$this->endBlockComment('row');

echo $output;