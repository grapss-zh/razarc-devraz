<?php
$output = $address = $size = $zoom = $type = $el_class = '';
extract(shortcode_atts(array( 
	'title' => '', 
	'message' => '', 
	'address' => '',
    'size' => 250,
    'zoom' => 14,
    'type' => 'ROADMAP',
	'email' => '',
	'phone' => '',
	'popup' => 'true',
	'scrollwheel' => 'true',
	'pan' => 'true',
	'zoom_control' => 'true',
	'type_control' => 'true',
	'streetview' => 'true',
    'el_class' => '',
	'css_animation' => ''
), $atts));

if ( $address == '' ) { return null; }

$el_class = $this->getExtraClass($el_class);
$bubble = ($bubble!='' && $bubble!='0') ? '&amp;iwloc=near' : '';

$map_id = uniqid( 'google_map_element_' );

$coordinates = get_coordinates( $address );

//$title = ($title != '') ? '<h3>'.$title.'</h3>' : '' ;

//$message = ($message != '') ? '<p>' . $message . '</p>' : '';

//$phone = ($phone != '') ? '<p class=\"nobottommargin\"><icon class=\"fa-phone\"></icon>&nbsp;&nbsp;' . $phone . '</p>' : '';

//$email = ($email != '') ? '<p class=\"nobottommargin\"><icon class=\"fa-envelope\"></icon>&nbsp;&nbsp;' . $email . '</p>' : '';

//$size = str_replace(array( 'px', ' ' ), array( '', '' ), $size);
$css_class =  apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'wpb_gmaps_widget wpb_content_element'.$el_class, $this->settings['base']);
$css_class .= $this->getCSSAnimation($css_animation);
	$lat = 32.802527;
	$lng = 34.981693;
	$zoom = 15;


$output .= "\n\t".'<div class="'.$css_class.'">';
$output .= "\n\t\t".'<div class="wpb_wrapper">';
$output .= ' <div class="googlemaps gmap" data-id="'.$map_id.'" data-lat='.$lat.' data-lng='.$lng.' data-address="'.$address.'" data-map="'.$type.'" data-zoom="'.$zoom.'" data-title="'.$title.'" data-message="'.$message.'" data-email="'.$email.'" data-phone="'.$phone.'" data-popup="'.$popup.'" data-scrollwheel="'.$scrollwheel.'" data-pan="'.$pan.'" data-zoom_control="'.$zoom_control.'" data-type_control="'.$type_control.'" data-streetview="'.$streetview.'">
                	<div id="'.$map_id.'" class="google_map_render" style="height:'.$size.'px">
                    </div>
                </div>';


$output .= "\n\t\t".'</div> '.$this->endBlockComment('.wpb_wrapper');
$output .= "\n\t".'</div> '.$this->endBlockComment('.wpb_gmaps_widget');

echo $output;