<?php
/**
 * WPBakery Visual Composer Shortcodes settings
 *
 * @package VPBakeryVisualComposer
 *
 */
$vc_is_wp_version_3_6_more = version_compare(preg_replace('/^([\d\.]+)(\-.*$)/', '$1', get_bloginfo('version')), '3.6') >= 0;
// Used in "Button" and "Call to Action" blocks
$colors_arr = array(__("Grey", "Nimva") => "wpb_button", __("Blue", "Nimva") => "btn-primary", __("Turquoise", "Nimva") => "btn-info", __("Green", "Nimva") => "btn-success", __("Orange", "Nimva") => "btn-warning", __("Red", "Nimva") => "btn-danger", __("Black", "Nimva") => "btn-inverse");

$button_colors = array( __("Red", "Nimva") => "red", __("Dark Red", "Nimva") => "dark_red", __("Light Blue", "Nimva") => "blue", __("Orange", "Nimva") => "orange", __("Turquoise", "Nimva") => "turquoise", __("Emerald", "Nimva") => "emerald", __("Amethyst", "Nimva") => "amethyst", __("Wet Asphalt", "Nimva") => "wet_asphalt", __("Light", "Nimva") => "light", __("Dark", "Nimva") => "dark", __("Transparent Light", "Nimva") => "transparent_light", __("Transparent Dark", "Nimva") => "transparent_dark");

// Used in "Button" and "Call to Action" blocks
$size_arr = array( __("Small", "Nimva") => "small", __("Large", "Nimva") => "large");

$button_style = array( __("Minimal", "Nimva") => "minimal", __("3D", "Nimva") => "3d");

$target_arr = array(__("Same window", "Nimva") => "_self", __("New window", "Nimva") => "_blank");

$alignment = array(__("Center", "Nimva") => "center", __("Left", "Nimva") => "left", __("Right", "Nimva") => "right" );

$posts = $categories = $custom_categories = $portfolio_posts = $clients = $testimonials = $faq = $employees = $pricings = array();

$posts_entries = get_posts( 'post_type=post&orderby=title&numberposts=-1&order=ASC' );
foreach ( $posts_entries as $key => $entry ) {
    $posts[$entry->ID] = $entry->post_title;
}

$cats_entries = get_categories( 'orderby=name&hide_empty=0' );
foreach ( $cats_entries as $key => $entry ) {
    $categories[$entry->term_id] = $entry->name;
}

global $wpdb;
  $tax = $wpdb->get_results( 
  	"
  	SELECT $wpdb->terms.term_id, $wpdb->terms.name, $wpdb->term_taxonomy.taxonomy 
  	FROM $wpdb->terms, $wpdb->term_taxonomy
  	WHERE $wpdb->term_taxonomy.taxonomy = 'portfolio_category' && $wpdb->terms.term_id = $wpdb->term_taxonomy.term_id 
  	"
  );
  
  $taxon = array();
  if ($tax) {
    foreach ( $tax as $taxonomy ) {
      $taxon[$taxonomy->term_id] = $taxonomy->name;
    }
  } else {
    $taxon["No portfolio category found"] = 0;
  }
  
  $tax2 = $wpdb->get_results( 
  	"
  	SELECT $wpdb->terms.term_id, $wpdb->terms.name, $wpdb->term_taxonomy.taxonomy 
  	FROM $wpdb->terms, $wpdb->term_taxonomy
  	WHERE $wpdb->term_taxonomy.taxonomy = 'faq_category' && $wpdb->terms.term_id = $wpdb->term_taxonomy.term_id 
  	"
  );
  
  $taxon2 = array();
  if ($tax2) {
    foreach ( $tax2 as $taxonomy ) {
      $taxon2[$taxonomy->term_id] = $taxonomy->name;
    }
  } else {
    $taxon2["No faq category found"] = 0;
  }  

$portfolio_entries = get_posts( 'post_type=creativo_portfolio&orderby=title&numberposts=-1&order=ASC' );
foreach ( $portfolio_entries as $key => $entry ) {
    $portfolio_posts[$entry->ID] = $entry->post_title;
}

$clients_entries = get_posts( 'post_type=clients&orderby=title&numberposts=-1&order=ASC' );

if ( $clients_entries != null && !empty( $clients_entries ) ) {
    foreach ( $clients_entries as $key => $entry ) {
        $clients[$entry->ID] = $entry->post_title;
    }
}

$testimonials_entries = get_posts( 'post_type=testimonials&orderby=title&numberposts=-1&order=ASC' );

if ( $testimonials_entries != null && !empty( $testimonials_entries ) ) {
    foreach ( $testimonials_entries as $key => $entry ) {
        $testimonials[$entry->ID] = $entry->post_title;
    }
}

$faq_entries = get_posts( 'post_type=faq&orderby=title&numberposts=-1&order=ASC' );

if ( $faq_entries != null && !empty( $faq_entries ) ) {
    foreach ( $faq_entries as $key => $entry ) {
        $faq[$entry->ID] = $entry->post_title;
    }
}

$pricings_entries = get_posts( 'post_type=pricing&orderby=title&numberposts=-1&order=ASC' );

if ( $pricings_entries != null && !empty( $pricings_entries ) ) {
    foreach ( $pricings_entries as $key => $entry ) {
        $pricings[$entry->ID] = $entry->post_title;
    }
}

$emp_entries = get_posts( 'post_type=employees&orderby=title&numberposts=-1&order=ASC' );

if ( $emp_entries != null && !empty( $emp_entries ) ) {
    foreach ( $emp_entries as $key => $entry ) {
        $employees[$entry->ID] = $entry->post_title;
    }
}


$awesome_icons_list = array (
	'' => '',
    'fa-glass' => "000",
	'fa-music' => "001",
	'fa-search' => "002",
	'fa-envelope-o' => "003",
	'fa-heart' => "004",
	'fa-star' => "005",
	'fa-star-o' => "006",
	'fa-user' => "007",
	'fa-film' => "008",
	'fa-th-large' => "009",
	'fa-th' => "00a",
	'fa-th-list' => "00b",
	'fa-check' => "00c",
	'fa-times' => "00d",
	'fa-search-plus' => "00e",
	'fa-search-minus' => "010",
	'fa-power-off' => "011",
	'fa-signal' => "012",
	'fa-cog' => "013",
	'fa-trash-o' => "014",
	'fa-home' => "015",
	'fa-file-o' => "016",
	'fa-clock-o' => "017",
	'fa-road' => "018",
	'fa-download' => "019",
	'fa-arrow-circle-o-down' => "01a",
	'fa-arrow-circle-o-up' => "01b",
	'fa-inbox' => "01c",
	'fa-play-circle-o' => "01d",
	'fa-repeat' => "01e",
	'fa-refresh' => "021",
	'fa-list-alt' => "022",
	'fa-lock' => "023",
	'fa-flag' => "024",
	'fa-headphones' => "025",
	'fa-volume-off' => "026",
	'fa-volume-down' => "027",
	'fa-volume-up' => "028",
	'fa-qrcode' => "029",
	'fa-barcode' => "02a",
	'fa-tag' => "02b",
	'fa-tags' => "02c",
	'fa-book' => "02d",
	'fa-bookmark' => "02e",
	'fa-print' => "02f",
	'fa-camera' => "030",
	'fa-font' => "031",
	'fa-bold' => "032",
	'fa-italic' => "033",
	'fa-text-height' => "034",
	'fa-text-width' => "035",
	'fa-align-left' => "036",
	'fa-align-center' => "037",
	'fa-align-right' => "038",
	'fa-align-justify' => "039",
	'fa-list' => "03a",
	'fa-outdent' => "03b",
	'fa-indent' => "03c",
	'fa-video-camera' => "03d",
	'fa-picture-o' => "03e",
	'fa-pencil' => "040",
	'fa-map-marker' => "041",
	'fa-adjust' => "042",
	'fa-tint' => "043",
	'fa-pencil-square-o' => "044",
	'fa-share-square-o' => "045",
	'fa-check-square-o' => "046",
	'fa-arrows' => "047",
	'fa-step-backward' => "048",
	'fa-fast-backward' => "049",
	'fa-backward' => "04a",
	'fa-play' => "04b",
	'fa-pause' => "04c",
	'fa-stop' => "04d",
	'fa-forward' => "04e",
	'fa-fast-forward' => "050",
	'fa-step-forward' => "051",
	'fa-eject' => "052",
	'fa-chevron-left' => "053",
	'fa-chevron-right' => "054",
	'fa-plus-circle' => "055",
	'fa-minus-circle' => "056",
	'fa-times-circle' => "057",
	'fa-check-circle' => "058",
	'fa-question-circle' => "059",
	'fa-info-circle' => "05a",
	'fa-crosshairs' => "05b",
	'fa-times-circle-o' => "05c",
	'fa-check-circle-o' => "05d",
	'fa-ban' => "05e",
	'fa-arrow-left' => "060",
	'fa-arrow-right' => "061",
	'fa-arrow-up' => "062",
	'fa-arrow-down' => "063",
	'fa-share' => "064",
	'fa-expand' => "065",
	'fa-compress' => "066",
	'fa-plus' => "067",
	'fa-minus' => "068",
	'fa-asterisk' => "069",
	'fa-exclamation-circle' => "06a",
	'fa-gift' => "06b",
	'fa-leaf' => "06c",
	'fa-fire' => "06d",
	'fa-eye' => "06e",
	'fa-eye-slash' => "070",
	'fa-exclamation-triangle' => "071",
	'fa-plane' => "072",
	'fa-calendar' => "073",
	'fa-random' => "074",
	'fa-comment' => "075",
	'fa-magnet' => "076",
	'fa-chevron-up' => "077",
	'fa-chevron-down' => "078",
	'fa-retweet' => "079",
	'fa-shopping-cart' => "07a",
	'fa-folder' => "07b",
	'fa-folder-open' => "07c",
	'fa-arrows-v' => "07d",
	'fa-arrows-h' => "07e",
	'fa-bar-chart-o' => "080",
	'fa-twitter-square' => "081",
	'fa-facebook-square' => "082",
	'fa-camera-retro' => "083",
	'fa-key' => "084",
	'fa-cogs' => "085",
	'fa-comments' => "086",
	'fa-thumbs-o-up' => "087",
	'fa-thumbs-o-down' => "088",
	'fa-star-half' => "089",
	'fa-heart-o' => "08a",
	'fa-sign-out' => "08b",
	'fa-linkedin-square' => "08c",
	'fa-thumb-tack' => "08d",
	'fa-external-link' => "08e",
	'fa-sign-in' => "090",
	'fa-trophy' => "091",
	'fa-github-square' => "092",
	'fa-upload' => "093",
	'fa-lemon-o' => "094",
	'fa-phone' => "095",
	'fa-square-o' => "096",
	'fa-bookmark-o' => "097",
	'fa-phone-square' => "098",
	'fa-twitter' => "099",
	'fa-facebook' => "09a",
	'fa-github' => "09b",
	'fa-unlock' => "09c",
	'fa-credit-card' => "09d",
	'fa-rss' => "09e",
	'fa-hdd-o' => "0a0",
	'fa-bullhorn' => "0a1",
	'fa-bell' => "0f3",
	'fa-certificate' => "0a3",
	'fa-hand-o-right' => "0a4",
	'fa-hand-o-left' => "0a5",
	'fa-hand-o-up' => "0a6",
	'fa-hand-o-down' => "0a7",
	'fa-arrow-circle-left' => "0a8",
	'fa-arrow-circle-right' => "0a9",
	'fa-arrow-circle-up' => "0aa",
	'fa-arrow-circle-down' => "0ab",
	'fa-globe' => "0ac",
	'fa-wrench' => "0ad",
	'fa-tasks' => "0ae",
	'fa-filter' => "0b0",
	'fa-briefcase' => "0b1",
	'fa-arrows-alt' => "0b2",
	
	'fa-users' => "0c0",

	'fa-link' => "0c1",
	'fa-cloud' => "0c2",
	'fa-flask' => "0c3",
	
	'fa-scissors' => "0c4",
	
	'fa-files-o' => "0c5",
	'fa-paperclip' => "0c6",
	
	'fa-floppy-o' => "0c7",
	'fa-square' => "0c8",
	'fa-bars' => "0c9",
	'fa-list-ul' => "0ca",
	'fa-list-ol' => "0cb",
	'fa-strikethrough' => "0cc",
	'fa-underline' => "0cd",
	'fa-table' => "0ce",
	'fa-magic' => "0d0",
	'fa-truck' => "0d1",
	'fa-pinterest' => "0d2",
	'fa-pinterest-square' => "0d3",
	'fa-google-plus-square' => "0d4",
	'fa-google-plus' => "0d5",
	'fa-money' => "0d6",
	'fa-caret-down' => "0d7",
	'fa-caret-up' => "0d8",
	'fa-caret-left' => "0d9",
	'fa-caret-right' => "0da",
	'fa-columns' => "0db",
	
	'fa-sort' => "0dc",
	
	'fa-sort-asc' => "0dd",
	
	'fa-sort-desc' => "0de",
	'fa-envelope' => "0e0",
	'fa-linkedin' => "0e1",
	
	'fa-undo' => "0e2",
	
	'fa-gavel' => "0e3",
	
	'fa-tachometer' => "0e4",
	'fa-comment-o' => "0e5",
	'fa-comments-o' => "0e6",
	
	'fa-bolt' => "0e7",
	'fa-sitemap' => "0e8",
	'fa-umbrella' => "0e9",
	
	'fa-clipboard' => "0ea",
	'fa-lightbulb-o' => "0eb",
	'fa-exchange' => "0ec",
	'fa-cloud-download' => "0ed",
	'fa-cloud-upload' => "0ee",
	'fa-user-md' => "0f0",
	'fa-stethoscope' => "0f1",
	'fa-suitcase' => "0f2",
	'fa-bell-o' => "0a2",
	'fa-coffee' => "0f4",
	'fa-cutlery' => "0f5",
	'fa-file-text-o' => "0f6",
	'fa-building-o' => "0f7",
	'fa-hospital-o' => "0f8",
	'fa-ambulance' => "0f9",
	'fa-medkit' => "0fa",
	'fa-fighter-jet' => "0fb",
	'fa-beer' => "0fc",
	'fa-h-square' => "0fd",
	'fa-plus-square' => "0fe",
	'fa-angle-double-left' => "100",
	'fa-angle-double-right' => "101",
	'fa-angle-double-up' => "102",
	'fa-angle-double-down' => "103",
	'fa-angle-left' => "104",
	'fa-angle-right' => "105",
	'fa-angle-up' => "106",
	'fa-angle-down' => "107",
	'fa-desktop' => "108",
	'fa-laptop' => "109",
	'fa-tablet' => "10a",
	
	'fa-mobile' => "10b",
	'fa-circle-o' => "10c",
	'fa-quote-left' => "10d",
	'fa-quote-right' => "10e",
	'fa-spinner' => "110",
	'fa-circle' => "111",
	
	'fa-reply' => "112",
	'fa-github-alt' => "113",
	'fa-folder-o' => "114",
	'fa-folder-open-o' => "115",
	'fa-smile-o' => "118",
	'fa-frown-o' => "119",
	'fa-meh-o' => "11a",
	'fa-gamepad' => "11b",
	'fa-keyboard-o' => "11c",
	'fa-flag-o' => "11d",
	'fa-flag-checkered' => "11e",
	'fa-terminal' => "120",
	'fa-code' => "121",
	'fa-reply-all' => "122",
	'fa-mail-reply-all' => "122",
	
	'fa-star-half-o' => "123",
	'fa-location-arrow' => "124",
	'fa-crop' => "125",
	'fa-code-fork' => "126",
	
	'fa-chain-broken' => "127",
	'fa-question' => "128",
	'fa-info' => "129",
	'fa-exclamation' => "12a",
	'fa-superscript' => "12b",
	'fa-subscript' => "12c",
	'fa-eraser' => "12d",
	'fa-puzzle-piece' => "12e",
	'fa-microphone' => "130",
	'fa-microphone-slash' => "131",
	'fa-shield' => "132",
	'fa-calendar-o' => "133",
	'fa-fire-extinguisher' => "134",
	'fa-rocket' => "135",
	'fa-maxcdn' => "136",
	'fa-chevron-circle-left' => "137",
	'fa-chevron-circle-right' => "138",
	'fa-chevron-circle-up' => "139",
	'fa-chevron-circle-down' => "13a",
	'fa-html5' => "13b",
	'fa-css3' => "13c",
	'fa-anchor' => "13d",
	'fa-unlock-alt' => "13e",
	'fa-bullseye' => "140",
	'fa-ellipsis-h' => "141",
	'fa-ellipsis-v' => "142",
	'fa-rss-square' => "143",
	'fa-play-circle' => "144",
	'fa-ticket' => "145",
	'fa-minus-square' => "146",
	'fa-minus-square-o' => "147",
	'fa-level-up' => "148",
	'fa-level-down' => "149",
	'fa-check-square' => "14a",
	'fa-pencil-square' => "14b",
	'fa-external-link-square' => "14c",
	'fa-share-square' => "14d",
	'fa-compass' => "14e",
	
	'fa-caret-square-o-down' => "150",
	
	'fa-caret-square-o-up' => "151",
	
	'fa-caret-square-o-right' => "152",
	
	'fa-eur' => "153",
	'fa-gbp' => "154",
	
	'fa-usd' => "155",
	
	'fa-inr' => "156",
	
	'fa-jpy' => "157",
	
	'fa-rub' => "158",
	
	'fa-krw' => "159",
	
	'fa-btc' => "15a",
	'fa-file' => "15b",
	'fa-file-text' => "15c",
	'fa-sort-alpha-asc' => "15d",
	'fa-sort-alpha-desc' => "15e",
	'fa-sort-amount-asc' => "160",
	'fa-sort-amount-desc' => "161",
	'fa-sort-numeric-asc' => "162",
	'fa-sort-numeric-desc' => "163",
	'fa-thumbs-up' => "164",
	'fa-thumbs-down' => "165",
	'fa-youtube-square' => "166",
	'fa-youtube' => "167",
	'fa-xing' => "168",
	'fa-xing-square' => "169",
	'fa-youtube-play' => "16a",
	'fa-dropbox' => "16b",
	'fa-stack-overflow' => "16c",
	'fa-instagram' => "16d",
	'fa-flickr' => "16e",
	'fa-adn' => "170",
	'fa-bitbucket' => "171",
	'fa-bitbucket-square' => "172",
	'fa-tumblr' => "173",
	'fa-tumblr-square' => "174",
	'fa-long-arrow-down' => "175",
	'fa-long-arrow-up' => "176",
	'fa-long-arrow-left' => "177",
	'fa-long-arrow-right' => "178",
	'fa-apple' => "179",
	'fa-windows' => "17a",
	'fa-android' => "17b",
	'fa-linux' => "17c",
	'fa-dribbble' => "17d",
	'fa-skype' => "17e",
	'fa-foursquare' => "180",
	'fa-trello' => "181",
	'fa-female' => "182",
	'fa-male' => "183",
	'fa-gittip' => "184",
	'fa-sun-o' => "185",
	'fa-moon-o' => "186",
	'fa-archive' => "187",
	'fa-bug' => "188",
	'fa-vk' => "189",
	'fa-weibo' => "18a",
	'fa-renren' => "18b",
	'fa-pagelines' => "18c",
	'fa-stack-exchange' => "18d",
	'fa-arrow-circle-o-right' => "18e",
	'fa-arrow-circle-o-left' => "190",
	
	'fa-caret-square-o-left' => "191",
	'fa-dot-circle-o' => "192",
	'fa-wheelchair' => "193",
	'fa-vimeo-square' => "194",	
	'fa-try' => "195",
	'fa-plus-square-o' => "196",
	'fa-space-shuttle' => "197",
	
	'fa-slack' => "198",
	'fa-envelope-square' => "199",
	'fa-wordpress' => "19a",
	'fa-openid' => "19b",
	//'fa-institution' => "19c",
	'fa-bank' => "19c",
	//'fa-university' => "19c",
	//'fa-mortar-board' => "19d",
	'fa-graduation-cap' => "19d",
	'fa-yahoo' => "19e",
	'fa-google' => "1a0",
	'fa-reddit' => "1a1",
	'fa-reddit-square' => "1a2",
	'fa-stumbleupon-circle' => "1a3",
	'fa-stumbleupon' => "1a4",
	'fa-delicious' => "1a5",
	'fa-digg' => "1a6",
	//'fa-pied-piper-square' => "1a7",
	'fa-pied-piper' => "1a7",
	'fa-pied-piper-alt' => "1a8",
	'fa-drupal' => "1a9",
	'fa-joomla' => "1aa",
	'fa-language' => "1ab",
	'fa-fax' => "1ac",
	'fa-building' => "1ad",
	'fa-child' => "1ae",
	'fa-paw' => "1b0",
	'fa-spoon' => "1b1",
	'fa-cube' => "1b2",
	'fa-cubes' => "1b3",
	'fa-behance' => "1b4",
	'fa-behance-square' => "1b5",
	'fa-steam' => "1b6",
	'fa-steam-square' => "1b7",
	'fa-recycle' => "1b8",
	//'fa-automobil' => "1b9",
	'fa-car' => "1b9",
	//'fa-cab' => "1ba",
	'fa-taxi' => "1ba",
	'fa-tree' => "1bb",
	'fa-spotify' => "1bc",
	'fa-deviantart' => "1bd",
	'fa-soundcloud' => "1be",
	'fa-database' => "1c0",
	'fa-file-pdf-o' => "1c1",
	'fa-file-word-o' => "1c2",
	'fa-file-excel-o' => "1c3",
	'fa-file-powerpoint-o' => "1c4",
	//'fa-file-photo-' => "1c5",
	'fa-file-picture-o' => "1c5",
	//'fa-file-image-o' => "1c5",
	'fa-file-zip-o' => "1c6",
	//'fa-file-archive-o' => "1c6",
	'fa-file-sound-o' => "1c7",
	//'fa-file-audio-o' => "1c7",
	'fa-file-movie-o' => "1c8",
	//'fa-file-video-o' => "1c8",
	'fa-file-code-o' => "1c9",
	'fa-vine' => "1ca",
	'fa-codepen' => "1cb",
	'fa-jsfiddle' => "1cc",
	//'fa-life-bou' => "1cd",
	//'fa-life-saver' => "1cd",
	'fa-support' => "1cd",
	//'fa-life-ring' => "1cd",
	'fa-circle-o-notch' => "1ce",
	//'fa-r' => "1d0",
	'fa-rebel' => "1d0",
	//'fa-g' => "1d1",
	'fa-empire' => "1d1",
	'fa-git-square' => "1d2",
	'fa-git' => "1d3",
	'fa-hacker-news' => "1d4",
	'fa-tencent-weibo' => "1d5",
	'fa-qq' => "1d6",
	//'fa-wecha' => "1d7",
	'fa-weixin' => "1d7",
	'fa-send' => "1d8",
	//'fa-paper-plane' => "1d8",
	'fa-send-o' => "1d9",
	'fa-paper-plane-o' => "1d9",
	'fa-history' => "1da",
	'fa-circle-thin' => "1db",
	'fa-header' => "1dc",
	'fa-paragraph' => "1dd",
	'fa-sliders' => "1de",
	'fa-share-alt' => "1e0",
	'fa-share-alt-square' => "1e1",
	'fa-bomb' => "1e2"	
);

$add_css_animation = array(
  "type" => "dropdown",
  "heading" => __("CSS Animation", "Nimva"),
  "param_name" => "css_animation",
  //"admin_label" => true,
  "value" => array(__("No", "Nimva") => '', __("Top to bottom", "Nimva") => "top-to-bottom", __("Bottom to top", "Nimva") => "bottom-to-top", __("Left to right", "Nimva") => "left-to-right", __("Right to left", "Nimva") => "right-to-left", __("Appear from center", "Nimva") => "appear"),
  "description" => __("Select animation type if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.", "Nimva")
);

vc_map( array(
  "name" => __("Row", "Nimva"),
  "base" => "vc_row",
  "is_container" => true,
  "icon" => "icon-wpb-row",
  "show_settings_on_create" => false,
  "category" => __('Content', 'Nimva'),
  "description" => __('Add a new row element', 'Nimva'),
  "params" => array(
    array(
      "type" => "dropdown",
      "heading" => __("Layout Width", "Nimva"),
      "param_name" => "layout",
      "value" => array(__('Normal', "Nimva") => "normal", __('Wide', "Nimva") => "wide"),
      "description" => __("Select the layout width.", "Nimva")
    ),
	array(
    	"type" => "range",
        "heading" => __( "Margin Top", "Nimva" ),
        "param_name" => "margintop",
        "value" => "0",
        "min" => "0",
        "max" => "100",
        "step" => "1",
        "unit" => 'px',

        "description" => __( "Select a value for the margin top. Leave 0 for no margin top", "Nimva" )
    ),
	array(
    	"type" => "range",
        "heading" => __( "Margin Bottom", "Nimva" ),
        "param_name" => "marginbottom",
        "value" => "0",
        "min" => "0",
        "max" => "100",
        "step" => "1",
        "unit" => 'px',

        "description" => __( "Select a value for the margin top. Leave 0 for no margin top", "Nimva" )
    ),
	array(
    	"type" => "range",
        "heading" => __( "Padding Top", "Nimva" ),
        "param_name" => "paddingtop",
        "value" => "30",
        "min" => "0",
        "max" => "100",
        "step" => "1",
        "unit" => 'px',

        "description" => __( "Select a value for the padding top. Leave 0 for no padding top", "Nimva" )
    ),
	array(
    	"type" => "range",
        "heading" => __( "Padding Bottom", "Nimva" ),
        "param_name" => "paddingbottom",
        "value" => "30",
        "min" => "0",
        "max" => "100",
        "step" => "1",
        "unit" => 'px',
        "description" => __( "Select a value for the padding bottom. Leave 0 for no padding bottom", "Nimva" )
    ),	
  	array(
      "type" => "colorpicker",
      "class" => "",
      "heading" => __("Background color", "vc_extend"),
      "param_name" => "bg_color",
      "value" => '', 
      "description" => __("Choose background color", "vc_extend")
    ),
	
	array(
    	"type" => "range",
        "heading" => __( "Border Width", "Nimva" ),
        "param_name" => "border_width",
        "value" => "0",
        "min" => "0",
        "max" => "10",
        "step" => "1",
        "unit" => 'px',
        "description" => __( "Select a value in pixels for the border width. Leave 0 for no border", "Nimva" )
    ),
	
	array(
      "type" => "colorpicker",
      "class" => "",
      "heading" => __("Border color", "Nimva"),
      "param_name" => "border_color",
      "value" => '#e8e8e8', 
      "description" => __("Choose border color", "Nimva")
    ),
	array(
      "type" => "attach_image",
      "heading" => __("Background Image", "Nimva"),
      "param_name" => "bg_image",
      "value" => "",
      "description" => __("Select an image to be used for the background of this row section.", "Nimva")
    ),
	array(
      "type" => "dropdown",
      "heading" => __("Background Attachment", "Nimva"),
      "param_name" => "bg_attachment",
      "value" => array(__('Scroll', "Nimva") => "scroll", __('Fixed', "Nimva") => "fixed"),
      "description" => __("The background-attachment property sets whether a background image is fixed or scrolls with the rest of the page. <a href=\"http://www.w3schools.com/CSSref/pr_background-attachment.asp\" target=\"_blank\">Read More</a>", "Nimva")
    ),
	array(
      "type" => "dropdown",
      "heading" => __("Background Repeat", "Nimva"),
      "param_name" => "bg_repeat",
      "value" => array(__('Repeat', "Nimva") => "repeat", __('No Repeat', "Nimva") => "no-repeat", __('Repeat Horizontally', "Nimva") => "repeat-x", __('Repeat Vertically', "Nimva") => "repeat-y")
    ),
	array(
      "type" => "switch",
	  "param_name" => "bg_stretch",
	  "value" => "false",
      "heading" => __("Stretch Background Image to fit the entire container", "Nimva")
    ),
	
	array(
      "type" => "switch",
	  "param_name" => "bg_parallax",
	  "value" => "false",
      "heading" => __("Enable Parallax effect for the background image?", "Nimva")
    ),
	array(
    	"heading" => __( "Background Pattern", 'Nimva' ),
    	"description" => __( "You can optionally select a pattern for this section background. This option only works when background image field is empty (above option).", 'Nimva' ),
        "param_name" => "bg_patterns",
        "border" => 'true',
        "value" => array(
                    'no-image.png' => "",
                    'patterns/pattern1.jpg' => "pattern1.jpg",
                    'patterns/pattern2.png' => "pattern2.png",
                    'patterns/pattern3.jpg' => "pattern3.jpg",
                    'patterns/pattern4.jpg' => "pattern4.jpg",
                    'patterns/pattern5.png' => "pattern5.png",
                    'patterns/pattern6.jpg' => "pattern6.jpg",
                    'patterns/pattern7.png' => "pattern7.png",
                    'patterns/pattern8.png' => "pattern8.png",
                    'patterns/pattern9.jpg' => "pattern9.jpg",
					'patterns/pattern10.png' => "pattern10.png",
                    'patterns/pattern11.jpg' => "pattern11.jpg",
					'patterns/pattern12.jpg' => "pattern12.jpg",
					'patterns/pattern13.jpg' => "pattern13.jpg",
					'patterns/pattern14.jpg' => "pattern14.jpg",
					'patterns/pattern15.png' => "pattern15.png",
					'patterns/pattern16.png' => "pattern16.png",
					'patterns/pattern17.png' => "pattern17.png",
					'patterns/pattern18.jpg' => "pattern18.jpg",
					'patterns/pattern19.jpg' => "pattern19.jpg",
					'patterns/pattern20.jpg' => "pattern20.jpg",
					'patterns/pattern21.jpg' => "pattern21.jpg",
					'patterns/pattern22.png' => "pattern22.png",
					'patterns/pattern23.jpg' => "pattern23.jpg",
					'patterns/pattern25.png' => "pattern25.png",
					'patterns/pattern26.png' => "pattern26.png",
					'patterns/pattern27.png' => "pattern27.png",
					'patterns/pattern28.png' => "pattern28.png",
					'patterns/pattern29.jpg' => "pattern29.jpg",
					'patterns/pattern31.png' => "pattern31.png",
					'patterns/pattern32.jpg' => "pattern32.jpg",
					'patterns/pattern33.jpg' => "pattern33.jpg",
					'patterns/pattern34.png' => "pattern34.png",
					'patterns/pattern35.jpg' => "pattern35.jpg",
					'patterns/pattern36.png' => "pattern36.png",
					'patterns/pattern37.png' => "pattern37.png",
					'patterns/pattern38.png' => "pattern38.png",
					'patterns/pattern39.png' => "pattern39.png",
					'patterns/pattern40.png' => "pattern40.png",
					'patterns/pattern41.jpg' => "pattern41.jpg",
					'patterns/pattern42.png' => "pattern42.png",
					'patterns/pattern43.png' => "pattern43.png",
					'patterns/pattern44.jpg' => "pattern44.jpg",
					'patterns/pattern45.png' => "pattern45.png",
					'patterns/pattern46.png' => "pattern46.png",
					'patterns/pattern47.png' => "pattern47.png",
					'patterns/pattern48.jpg' => "pattern48.jpg",
					'patterns/pattern49.jpg' => "pattern49.jpg",
					'patterns/pattern50.jpg' => "pattern50.jpg",
					'patterns/pattern51.png' => "pattern51.png",
					
                ),
                "type" => "visual_selector"
            ),
	array(
      "type" => "switch",
	  "param_name" => "video_bg",
	  "value" => "false",
      "heading" => __("Enable Video Background", "Nimva"),
      "description" => __("Enable or disable Youtube Video Background")
    ),
	array(
      "type" => "textfield",
      "heading" => __("Youtube Video ID", "Nimva"),
      "param_name" => "ytb_video_id",
      "description" => __("Paste here the id of the YouTube video you want to use. 
E.g: https://www.youtube.com/watch?v=PZizZxwrrbk - if this is your video link, the id would be: PZizZxwrrbk", "Nimva")
    ),
	
	array(
      "type" => "dropdown",
      "heading" => __("Video Quality", "Nimva"),
      "param_name" => "video_quality",
      "value" => array(__("Default", "Nimva") => "default", __("Small", "Nimva") => "small", __("Medium", "Nimva") => "medium",  __("Large", "Nimva") => "large",  __("720 HD", "Nimva") => "hd720",  __("1080 HD", "Nimva") => "hd1080",  __("High Resolution", "Nimva") => "highres"),
      "description" => __("Select button type.", "Nimva")
    ),
	
	array(
    	"type" => "range",
        "heading" => __( "Video Opacity", "Nimva" ),
        "param_name" => "vid_opacity",
        "value" => "70",
        "min" => "0",
        "max" => "100",
        "step" => "5",
		"read" => "readonly",
        "unit" => '%',
        "description" => __( "Select the video opacity, in percents. ", "Nimva" )
    ),
	
	array(
      "type" => "colorpicker",
      "class" => "",
      "heading" => __("Video Overlay Color", "Nimva"),
      "param_name" => "video_overlay_col",
      "value" => '', //Default White color
      "description" => __("Add a colored background over the Video Background. Leave blank if you don't want to use this feature.", "Nimva")
    ),
	
	array(
    	"type" => "range",
        "heading" => __( "Video Overlay Opacity", "Nimva" ),
        "param_name" => "video_overlay_opac",
        "value" => "70",
        "min" => "0",
        "max" => "100",
        "step" => "5",
		"read" => "readonly",
        "unit" => '%',
        "description" => __( "Select the opacity of the colored background, in percents. ", "Nimva" )
    ),
	
	array(
    	"heading" => __( "Background Video Overlay Pattern", 'Nimva' ),
    	"description" => __( "You can optionally select an overlay pattern for your video background.", 'Nimva' ),
        "param_name" => "video_overlay",
        "border" => 'true',
        "value" => array(
                    'no-image.png' => "",
                    'overlay/overlay-pattern1.png' => "overlay-pattern1.png",
                    'overlay/overlay-pattern2.png' => "overlay-pattern2.png",
					'overlay/overlay-pattern3.png' => "overlay-pattern3.png",
					'overlay/overlay-pattern4.png' => "overlay-pattern4.png",
                ),
                "type" => "visual_selector"
            ),	
	
	array(
      "type" => "colorpicker",
      "class" => "",
      "heading" => __("Font color", "Nimva"),
      "param_name" => "row_font_color",
      "value" => '', //Default White color
      "description" => __("Choose font color for this row. Leave blank to use the default font color defined in Theme Options.", "Nimva")
    ),
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  ),
  "js_view" => 'VcRowView'
) );
vc_map( array(
  "name" => __("Row", "Nimva"), //Inner Row
  "base" => "vc_row_inner",
  "content_element" => false,
  "is_container" => true,
  "icon" => "icon-wpb-row",
  "show_settings_on_create" => false,
  "params" => array(
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  ),
  "js_view" => 'VcRowView'
) );
vc_map( array(
  "name" => __("Column", "Nimva"),
  "base" => "vc_column",
  "is_container" => true,
  "content_element" => false,
  "params" => array(
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  ),
  "js_view" => 'VcColumnView'
) );

/* Title
---------------------------------------------------------- */

vc_map( array(
  "name" => __("Title", "Nimva"),
  "base" => "ctitle",
  "class" => "",
  "controls" => "full",
  "icon" => "icon-wpb-title",
  "category" => __('Content', 'Nimva'),
  "description" => __('Add a cool title to your section', 'Nimva'),
  //'admin_enqueue_js' => array(plugins_url('vc_extend_admin.js', __FILE__)),
//  'admin_enqueue_css' => array(plugins_url('vc_extend_admin.css', __FILE__)),
  "params" => array(
    array(
      "type" => "textfield",
      "holder" => "div",
      "class" => "",
      "heading" => __("Title", "Nimva"),
      "param_name" => "title",
      "value" => __("Your title here", "Nimva"),
      "description" => __("Add a title to a section.", "Nimva")
    ),
	array(
      "type" => "colorpicker",
      "holder" => "div",
      "class" => "",
      "heading" => __("Title color", "Nimva"),
      "param_name" => "color",
      "value" => '', //Default Red color
      "description" => __("Choose title color. Leave blank if you want to use the default color set in Theme Options.", "Nimva")
    ),
	array(
    	"type" => "range",
        "heading" => __( "Title Font Size", "Nimva" ),
        "param_name" => "font_size",
        "value" => "11",
        "min" => "10",
        "max" => "30",
        "step" => "1",
        "unit" => 'px',
        "description" => __( "Select the font size for the Title. ", "Nimva" )
    ),
	array(
      "type" => "colorpicker",
      "holder" => "div",
      "class" => "",
      "heading" => __("Title small border bottom color", "Nimva"),
      "param_name" => "title_border",
      "value" => '', //Default Red color
      "description" => __("Choose the color of the bottom border. Leave blank if you want to use the default color set in Theme Options", "Nimva")
    ),
	array(
      "type" => "colorpicker",
      "holder" => "div",
      "class" => "",
      "heading" => __("Title big border bottom color", "Nimva"),
      "param_name" => "title_border_big",
      "value" => '', //Default Red color
      "description" => __("Choose the color of the big bottom border. Leave blank if you want to use the default color set in Theme Options", "Nimva")
    ),
	array(
      "type" => "switch",
	  "param_name" => "big_border_en",
	  "value" => "true",
      "heading" => __("Show Title Big Border?", "Nimva"),
      "description" => __("Enable or disable the title big border.")
    ),
	
	array(
      "type" => "switch",
	  "param_name" => "uppercase",
	  "value" => "true",
      "heading" => __("Title Uppercase", "Nimva"),
      "description" => __("Enable or disable title uppercase.")
    ),
	
	array(
      "type" => "dropdown",
      "heading" => __("Title Position", "Nimva"),
      "param_name" => "position",
      "value" => array(__('Left', "Nimva") => "left", __('Center', "Nimva") => "center", __('Right', "Nimva") => "right" ),
      "description" => __("Select the positioning of the Title element.", "Nimva")
    ),
	
	array(
    	"type" => "awesome_icons",
        "heading" => __( "Title Icon", "Nimva" ),
        "param_name" => "icon",
		"holder" => "div",
        "width" => 200,
        "value" => $awesome_icons_list,
        "encoding" => "false",
        "description" => __( "Optionally you can insert an icon for the title.", "Nimva" )
    ),
	
	$add_css_animation,
	
	array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  ),
  "js_view" => 'VcCoolTitle'
) );

/* Text Block
---------------------------------------------------------- */
vc_map( array(
  "name" => __("Text Block", "Nimva"),
  "base" => "vc_column_text",
  "icon" => "icon-wpb-layer-shape-text",
  "wrapper_class" => "clearfix",
  "category" => __('Content', 'Nimva'),
  "description" => __('A block of text with WYSIWYG editor', 'Nimva'),
  "params" => array(
    array(
      "type" => "textarea_html",
      "holder" => "div",
      "heading" => __("Text", "Nimva"),
      "param_name" => "content",
      "value" => __("<p>I am text block. Click edit button to change this text.</p>", "Nimva")
    ),	 
    $add_css_animation,
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  )
) );


/* Quote Box
---------------------------------------------------------- */

vc_map( array(
    "name" => __("Quote Box", "Nimva"),
    "base" => "qbox",
    "class" => "",
    "icon" => "icon-wpb-qbox",
    "category" => __('Content', 'Nimva'),
	"description" => __('Add Quote Box to your site.', 'Nimva'),
    "params" => array(	
		
		array(
		  "type" => "textfield",
		  "holder" => "h2",
		  "class" => "",
		  "heading" => __(" Title", 'Creativo'),
		  "param_name" => "title1",
		  "value" => "",
		  "description" => __("Enter text for the title.", 'Creativo')
		),
		array(
		  "type" => "textarea",
		  "holder" => "div",
		  "class" => "",
		  "heading" => __("Description", 'Creativo'),
		  "param_name" => "title2",
		  "value" => "",
		  "description" => __("Enter a brief description that will appear right below the title.", 'Creativo')
		),
		
		array(
			"type" => "dropdown",
			"heading" => __("Text align", "Nimva"),
			"param_name" => "align",
			"value" => array( __("Center", "Nimva") => 'center', __("Left", "Nimva") => 'left', __("Right", "Nimva") => 'right' ),
		),
				
		array(
			"type" => "range",
			"heading" => __( "Title Font Size", "Nimva" ),
			"param_name" => "big_size",
			"value" => "26",
			"min" => "0",
			"max" => "50",
			"step" => "1",
			"unit" => 'px',		
		),
		array(
		  "type" => "colorpicker",
		  "heading" => __("Title color", 'Nimva'),
		  "param_name" => "big_color",
		  "value" => '#777',
		  "description" => __("Select the color of the big title", 'Nimva')
		),
		array(
			"type" => "range",
			"heading" => __( "Description Font Size", "Nimva" ),
			"param_name" => "small_size",
			"value" => "15",
			"min" => "0",
			"max" => "50",
			"step" => "1",
			"unit" => 'px',			
		),
		array(
		  "type" => "colorpicker",
		  "heading" => __("Description Font Color", 'Nimva'),
		  "param_name" => "small_color",
		  "value" => '#777', 
		  "description" => __("Select the color of the big title", 'Nimva')
		),
		array(
			"type" => "dropdown",
			"heading" => __("Enable Buttons", "Nimva"),
			"param_name" => "en_buttons",
			"value" => array( __("No", "Nimva") => 'false', __("Yes", "Nimva") => 'true' ),
		),
		
		array(
		  "type" => "dropdown",
		  "heading" => __("Button 1 Style", "Nimva"),
		  "param_name" => "b1_style",		  
		  "dependency" => Array('element' => 'en_buttons', 'value' => array('true')),
		  "value" => $button_style
		),
		array(
		  "type" => "textfield",
		  "heading" => __("Text on the button 1", "Nimva"),
		  //"holder" => "button",
		  //"class" => "wpb_button",
		  "param_name" => "b1_title",
		  "dependency" => Array('element' => 'en_buttons', 'value' => array('true')),
		  "value" => __("Text on the button", "Nimva"),
		),
		array(
			"type" => "awesome_icons",
			"heading" => __( "Button 1 Icon", "Nimva" ),
			"param_name" => "b1_icon",
			"width" => 200,
			"dependency" => Array('element' => 'en_buttons', 'value' => array('true')),
			"value" => $awesome_icons_list,
			"encoding" => "false"
		),
		array(
		  "type" => "textfield",
		  "heading" => __("Button 1 URL (Link)", "Nimva"),
		  "param_name" => "b1_href",
		  "dependency" => Array('element' => 'en_buttons', 'value' => array('true'))
		),

		array(
		  "type" => "dropdown",
		  "heading" => __("Button 1 Color", "Nimva"),
		  "param_name" => "b1_color",
		  "dependency" => Array('element' => 'en_buttons', 'value' => array('true')),
		  "value" => $button_colors
		),
		
		array(
		  "type" => "switch",
		  "heading" => __("Button 1 Inverse Color", "Nimva"),
		  "param_name" => "b1_inverse",
		  "dependency" => Array('element' => 'en_buttons', 'value' => array('true')),
		  "value" => "false",
		  "description" => __("This option will only work if you select the Minimal style for the button.", "Nimva")
		),
		
		array(
		  "type" => "dropdown",
		  "heading" => __("Button 1 Size", "Nimva"),
		  "param_name" => "b1_size",
		  "dependency" => Array('element' => 'en_buttons', 'value' => array('true')),
		  "value" => $size_arr,
		),
		
		array(
		  "type" => "dropdown",
		  "heading" => __("Button 2 Style", "Nimva"),
		  "param_name" => "b2_style",
		  "dependency" => Array('element' => 'en_buttons', 'value' => array('true')),
		  "value" => $button_style
		),
		array(
		  "type" => "textfield",
		  "heading" => __("Text on the button 2", "Nimva"),
		  //"holder" => "button",
		  //"class" => "wpb_button",
		  "param_name" => "b2_title",
		  "dependency" => Array('element' => 'en_buttons', 'value' => array('true')),
		  "value" => __("Text on the button", "Nimva"),
		),
		array(
			"type" => "awesome_icons",
			"heading" => __( "Button 2 Icon", "Nimva" ),
			"param_name" => "b2_icon",
			"width" => 200,
			"dependency" => Array('element' => 'en_buttons', 'value' => array('true')),
			"value" => $awesome_icons_list,
			"encoding" => "false"
		),
		array(
		  "type" => "textfield",
		  "heading" => __("Button 2 URL (Link)", "Nimva"),
		  "param_name" => "b2_href",
		  "dependency" => Array('element' => 'en_buttons', 'value' => array('true'))
		),
		
		array(
		  "type" => "dropdown",
		  "heading" => __("Button 2 Color", "Nimva"),
		  "param_name" => "b2_color",
		  "dependency" => Array('element' => 'en_buttons', 'value' => array('true')),
		  "value" => $button_colors
		),

		array(
		  "type" => "switch",
		  "heading" => __("Button 2 Inverse Color?", "Nimva"),
		  "param_name" => "b2_inverse",
		  "dependency" => Array('element' => 'en_buttons', 'value' => array('true')),
		  "value" => "false",
		  "description" => __("This option will only work if you select the Minimal style for the button.", "Nimva")
		),
		
		array(
		  "type" => "dropdown",
		  "heading" => __("Button 2 Size", "Nimva"),
		  "param_name" => "b2_size",
		  "dependency" => Array('element' => 'en_buttons', 'value' => array('true')),
		  "value" => $size_arr,
		),
		
		$add_css_animation,
		
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "Nimva"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
        ),

    ),
	"js_view" => 'VcQuoteBox'
) );

vc_map( array(
  "name" => __("Button", "Nimva"),
  "base" => "vc_button",
  "icon" => "icon-wpb-ui-button",
  "category" => __('Content', 'Nimva'),
  "description" => __('Eye catching button', 'Nimva'),
  "params" => array(
  	array(
      "type" => "dropdown",
      "heading" => __("Style", "Nimva"),
      "param_name" => "style",
      "value" => $button_style
    ),
    array(
      "type" => "textfield",
      "heading" => __("Text on the button", "Nimva"),
      "holder" => "button",
      "class" => "wpb_button",
      "param_name" => "title",
      "value" => __("Text on the button", "Nimva"),
      "description" => __("Text on the button.", "Nimva")
    ),
	array(
    	"type" => "awesome_icons",
        "heading" => __( "Icon", "Nimva" ),
        "param_name" => "icon",
        "width" => 200,
        "value" => $awesome_icons_list,
        "encoding" => "false",
        "description" => __( "Optionally you can insert an icon for the title.", "Nimva" )
    ),
    array(
      "type" => "textfield",
      "heading" => __("URL (Link)", "Nimva"),
      "param_name" => "href",
      "description" => __("Button link.", "Nimva")
    ),
    array(
      "type" => "dropdown",
      "heading" => __("Target", "Nimva"),
      "param_name" => "target",
      "value" => $target_arr,
      "dependency" => Array('element' => "href", 'not_empty' => true)
    ),
    array(
      "type" => "dropdown",
      "heading" => __("Button Color", "Nimva"),
      "param_name" => "color",
      "value" => $button_colors,
      "description" => __("Button color.", "Nimva")
    ),
	array(
      "type" => "switch",
	  "param_name" => "button_inverse",
	  "value" => "false",
      "heading" => __("Inverse button colors?", "Nimva"),      
	  "dependency" => Array('element' => "style", 'value' => array('minimal'))
    ),

    array(
      "type" => "dropdown",
      "heading" => __("Size", "Nimva"),
      "param_name" => "size",
      "value" => $size_arr,
      "description" => __("Button size.", "Nimva")
    ),
	
	array(
      "type" => "dropdown",
      "heading" => __("Align", "Nimva"),
      "param_name" => "align",
      "value" => $alignment,
      "description" => __("Button alignment.", "Nimva")
    ),
	
	$add_css_animation,
	
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  ),
  "js_view" => 'VcButtonView'
) );

/* Call to Action Button
---------------------------------------------------------- */
vc_map( array(
  "name" => __("Call to Action Box", "Nimva"),
  "base" => "tagline_box",
  "icon" => "icon-wpb-call-to-action",
  "category" => __('Content', 'Nimva'),
  "description" => __('Catch visitors attention with CTA block', 'Nimva'),
  "params" => array(
  
	array(
      "type" => "dropdown",
      "heading" => __("Call to Action style", "Nimva"),
      "param_name" => "cta_style",
      "value" => array(__("Normal", "Nimva") => "normal", __("Centered", "Nimva") => "center"),
      "description" => __("Select a style for the Call to Action.", "Nimva")
    ),	
	  
    array(
      "type" => "textarea",
      'admin_label' => true,
      "heading" => __("Heading Text", "Nimva"),
      "param_name" => "call_text",
      "value" => "",
      "description" => __("Enter your Title for the Call to Action.", "Nimva")
    ),
	
	array(
		"type" => "range",
		"heading" => __( "Heading Text font size", "Nimva" ),
		"param_name" => "call_text_size",
		"description" => __('Select the size of the Heading Text in pixels.', 'Nimva'),
		"value" => "19",
		"min" => "10",
		"max" => "50",
		"step" => "1",
		"unit" => 'pixels',
		"read" => "readonly"					
	),
	
	array(
      "type" => "colorpicker",
      "heading" => __("Heading Text color", "Nimva"),
      "param_name" => "heading_color",
      "description" => __("Select a custom color for the title. Leave blank to use the color set in the Theme Options -> Shortcodes -> Call to Action", "Nimva"),
      "dependency" => Array('element' => "bgcolor", 'value' => array('custom'))
    ),
	array(
      "type" => "textarea_html",
      'admin_label' => true,
      "heading" => __("Description", "Nimva"),
      "param_name" => "content",
      "value" => "",
      "description" => __("Enter your content.", "Nimva")
    ),
	
	array(
		"type" => "range",
		"heading" => __( "Description Text font size", "Nimva" ),
		"param_name" => "desc_text_size",
		"description" => __('Select the size of the Description in pixels.', 'Nimva'),
		"value" => "12",
		"min" => "10",
		"max" => "50",
		"step" => "1",
		"unit" => 'pixels',
		"read" => "readonly"					
	),
	
	array(
      "type" => "colorpicker",
      "heading" => __("Description color", "Nimva"),
      "param_name" => "description_color",
      "description" => __("Select a custom color for the description. <br />Leave blank to use the color set in the Theme Options -> Shortcodes -> Call to Action", "Nimva"),
    ),
	
	array(
      "type" => "colorpicker",
      "heading" => __("Background color", "Nimva"),
      "param_name" => "bg_color",
      "description" => __("Select a custom background color for the Call to Action box. <br />Leave blank to use the color set in the Theme Options -> Shortcodes -> Call to Action", "Nimva"),
    ),
	
	array(
      "type" => "colorpicker",
      "heading" => __("Border color", "Nimva"),
      "param_name" => "border_color",
      "description" => __("Select a custom border color for the Call to Action box. <br />Leave blank to use the color set in the Theme Options -> Shortcodes -> Call to Action", "Nimva"),
    ),
	
	array(
      "type" => "colorpicker",
      "heading" => __("Inner border color", "Nimva"),
      "param_name" => "inner_border_color",
      "description" => __("Select a custom color for the inner border of the Call to Action box. <br />Leave blank to use the color set in the Theme Options -> Shortcodes -> Call to Action", "Nimva"),
    ),
	
	array(
		"type" => "switch",
		"heading" => __("Force Transparency", "Nimva"),
		"param_name" => "force_transparency",
		"value" => "false",
		"description" => __( "Make the background of your Call to Action box fully transparent. Only the text and button will be visible. Useful when using a background image/pattern on the row element.", "Nimva" )
	),
	
	array(
		"type" => "switch",
		"heading" => __("Add shadow", "Nimva"),
		"param_name" => "cta_shadow",
		"value" => "false",
		"description" => __( "Add shadow effect to the Call to Action box", "Nimva" )
	),
	
    array(
      "type" => "textfield",
      "heading" => __("Text on the button", "Nimva"),
      "param_name" => "title",
      "value" => __("Text on the button", "Nimva"),
      "description" => __("Text on the button.", "Nimva")
    ),
	array(
      "type" => "dropdown",
      "heading" => __("Style", "Nimva"),
      "param_name" => "style",
      "value" => $button_style
    ),
    array(
      "type" => "dropdown",
      "heading" => __("Color", "Nimva"),
      "param_name" => "color",
      "value" => $button_colors,
      "description" => __("Button color.", "Nimva")
    ),
	
	array(
      "type" => "textfield",
      "heading" => __("URL (Link)", "Nimva"),
      "param_name" => "href",
      "description" => __("Button link.", "Nimva")
    ),
    array(
      "type" => "dropdown",
      "heading" => __("Target", "Nimva"),
      "param_name" => "target",
      "value" => $target_arr,
      "dependency" => Array('element' => "href", 'not_empty' => true)
    ),
	
    array(
      "type" => "dropdown",
      "heading" => __("Size", "Nimva"),
      "param_name" => "size",
      "value" => $size_arr,
      "description" => __("Button size.", "Nimva")
    ),
	array(
    	"type" => "awesome_icons",
        "heading" => __( "Icon", "Nimva" ),
        "param_name" => "icon",
        "width" => 200,
        "value" => $awesome_icons_list,
        "encoding" => "false",
        "description" => __( "Optionally you can insert an icon for the title.", "Nimva" )
    ), 
	
	$add_css_animation,   

    array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  ),
  "js_view" => 'VcCallToActionView'
) );

/* Separator (Divider)
---------------------------------------------------------- */
vc_map( array(
  "name"		=> __("Separator", "Nimva"),
  "base"		=> "vc_separator",
  'icon'		=> 'icon-wpb-ui-separator',
  "category"  => __('Content', 'Nimva'),
  "description" => __('Add a horizontal separator', 'Nimva'),
  "params" => array(
  	array(
      "type" => "dropdown",
      "heading" => __("Style", "Nimva"),
      "param_name" => "style",
      "value" => array( __('Dotted', "Nimva") => "dotted", 
	  					__('Double Dotted', "Nimva") => "double_dotted", 
						__('Solid', "Nimva") => "solid", 
						__('Double Solid', "Nimva") => "double_solid",
						__('Dashed', "Nimva") => "dashed",
						__('Double Dashed', "Nimva") => "double_dashed",
						__('Shadow', "Nimva") => "shadow_line",
						__('Blank Divider', "Nimva") => "blank_divider"
				), 
						
      "description" => __("Select the style of the separator.", "Nimva")
    ),
	array(
      "type" => "colorpicker",
      "class" => "",
      "heading" => __("Divider color", "Nimva"),
      "param_name" => "divider_color",
      "value" => '', //Default White color
      "description" => __("Choose a color for the divider.", "Nimva")
    ),
	array(
    	"type" => "range",
        "heading" => __( "Padding Top", "Nimva" ),
        "param_name" => "paddingtop",
        "value" => "20",
        "min" => "0",
        "max" => "100",
        "step" => "1",
        "unit" => 'px',
        "description" => __( "Select a value for the top margin. ", "Nimva" )
    ),
	array(
    	"type" => "range",
        "heading" => __( "Padding Bottom", "Nimva" ),
        "param_name" => "paddingbottom",
        "value" => "20",
        "min" => "0",
        "max" => "100",
        "step" => "1",
        "unit" => 'px',
        "description" => __( "Select a value for the bottom padding. ", "Nimva" )
    ), 
	array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    ) 		
  ),
  "js_view" => 'VcDivider'
) );


/* Message box
---------------------------------------------------------- */
vc_map( array(
  "name" => __("Message Box", "Nimva"),
  "base" => "vc_message",
  "icon" => "icon-wpb-information-white",
  "wrapper_class" => "alert",
  "category" => __('Content', 'Nimva'),
  "description" => __('Add a notification box', 'Nimva'),
  "params" => array(
    array(
      "type" => "dropdown",
      "heading" => __("Message box type", "Nimva"),
      "param_name" => "color",
      "value" => array(__('General', "Nimva") => "info", __('Error', "Nimva") => "error", __('Success', "Nimva") => "success", __('Warning', "Nimva") => "warning"),
      "description" => __("Select message type.", "Nimva")
    ),
	array(
      "type" => "textfield",
      "heading" => __("Title", "Nimva"),
      "param_name" => "title",
      "description" => __("Add a title for your message box.", "Nimva")
    ),	
    array(
      "type" => "textarea_html",
      "holder" => "div",
      "class" => "messagebox_text",
      "heading" => __("Message text", "Nimva"),
      "param_name" => "content",
      "value" => __("<p>I am message box. Click edit button to change this text.</p>", "Nimva")
    ),
	array(
      "type" => "switch",
	  "param_name" => "dismiss",
	  "value" => "true",
      "heading" => __("Allow message to be dismissed", "Nimva"),
      "description" => __("Give your visitors the ability to dismiss the message box or not.")
    ),
	array(
    	"type" => "awesome_icons",
        "heading" => __( "Icon", "Nimva" ),
        "param_name" => "icon",
        "width" => 200,
        "value" => $awesome_icons_list,
        "encoding" => "false",
        "description" => __( "Optionally you can insert an icon for the message box.", "Nimva" )
    ),
    $add_css_animation,
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  ),
  "js_view" => 'VcMessageView'
) );



/* Single image */
vc_map( array(
  "name" => __("Single Image", "Nimva"),
  "base" => "vc_single_image",
  "icon" => "icon-wpb-single-image",
  "category" => __('Content', 'Nimva'),
  "description" => __('Simple image with CSS animation', 'Nimva'),
  "params" => array(
    array(
      "type" => "textfield",
      "heading" => __("Widget title", "Nimva"),
      "param_name" => "title",
      "description" => __("What text use as a widget title. Leave blank if no title is needed.", "Nimva")
    ),
    array(
      "type" => "attach_image",
      "heading" => __("Image", "Nimva"),
      "param_name" => "image",
      "value" => "",
      "description" => __("Select image from media library.", "Nimva")
    ),
    
    array(
      "type" => "textfield",
      "heading" => __("Image size", "Nimva"),
      "param_name" => "img_size",
      "description" => __("Enter image size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height). Leave empty to use 'thumbnail' size.", "Nimva")
    ),
	array(
      "type" => "dropdown",
      "heading" => __("Image alignment", "Nimva"),
      "param_name" => "alignment",
      "value" => array(__("Align left", "Nimva") => "", __("Align right", "Nimva") => "right", __("Align center", "Nimva") => "center"),
      "description" => __("Select image alignment.", "Nimva")
    ),
    array(
      "type" => 'checkbox',
      "heading" => __("Link to large image?", "Nimva"),
      "param_name" => "img_link_large",
      "description" => __("If selected, image will be linked to the bigger image.", "Nimva"),
      "value" => Array(__("Yes, please", "Nimva") => 'yes')
    ),
   
	array(
      "type" => "textfield",
      "heading" => __("Image link", "Nimva"),
      "param_name" => "img_link",
      "description" => __("Enter url if you want this image to have link.", "Nimva"),
      "dependency" => Array('element' => "img_link_large", 'is_empty' => true, 'callback' => 'wpb_single_image_img_link_dependency_callback')
    ),
	
	 array(
      "type" => "dropdown",
      "heading" => __("Link Target", "Nimva"),
      "param_name" => "img_link_target",
      "value" => $target_arr,
      "dependency" => Array('element' => "img_link", 'not_empty' => true)
    ),
	
	$add_css_animation,
	
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  )
));

/* Gallery/Slideshow
---------------------------------------------------------- */
vc_map( array(
  "name" => __("Image Gallery", "Nimva"),
  "base" => "vc_gallery",
  "icon" => "icon-wpb-images-stack",
  "category" => __('Content', 'Nimva'),
  "description" => __('Add an image gallery or slider', 'Nimva'),
  "params" => array(
    array(
      "type" => "textfield",
      "heading" => __("Widget title", "Nimva"),
      "param_name" => "title",
      "description" => __("What text use as a widget title. Leave blank if no title is needed.", "Nimva")
    ),
    array(
      "type" => "dropdown",
      "heading" => __("Gallery type", "Nimva"),
      "param_name" => "type",
      "value" => array(__("Flex slider fade", "Nimva") => "flexslider_fade", __("Flex slider slide", "Nimva") => "flexslider_slide", __("Image grid", "Nimva") => "image_grid"),
      "description" => __("Select gallery type.", "Nimva")
    ),
    array(
      "type" => "dropdown",
      "heading" => __("Auto rotate slides", "Nimva"),
      "param_name" => "interval",
      "value" => array(3, 5, 10, 15, __("Disable", "Nimva") => 0),
      "description" => __("Auto rotate slides each X seconds.", "Nimva"),
      "dependency" => Array('element' => "type", 'value' => array('flexslider_fade', 'flexslider_slide'))
    ),
    array(
      "type" => "attach_images",
      "heading" => __("Images", "Nimva"),
      "param_name" => "images",
      "value" => "",
      "description" => __("Select images from media library.", "Nimva")
    ),
    array(
      "type" => "textfield",
      "heading" => __("Slider Width", "Nimva"),
      "param_name" => "slider_width",
	  "dependency" => Array('element' => "type", 'value' => array('flexslider_fade', 'flexslider_slide')),
      "description" => __("Enter slider width size in pixels. Example: 200px, 500px.", "Nimva")
    ),
	array(
      "type" => "textfield",
      "heading" => __("Image size", "Nimva"),
      "param_name" => "img_size",
      "description" => __("Enter image size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height). Leave empty to use 'thumbnail' size.", "Nimva")
    ),	
	array(
    	"type" => "range",
        "heading" => __( "Image border", "Nimva" ),
        "param_name" => "image_border",
        "value" => "0",
        "min" => "0",
        "max" => "10",
        "step" => "1",
        "unit" => 'px',
		"dependency" => Array('element' => "type", 'value' => "image_grid"),
        "description" => __( "Add a border to your images. Select 0 if you don't want to show any border.", "Nimva" )
    ),
	array(
      "type" => "colorpicker",
      "class" => "",
      "heading" => __("Border color", "Nimva"),
      "param_name" => "border_color",
      "value" => '', //Default White color
	  "dependency" => Array('element' => "type", 'value' => "image_grid"),
      "description" => __("Choose a color for the border of the images.", "Nimva")
    ),
    array(
      "type" => "dropdown",
      "heading" => __("On click", "Nimva"),
      "param_name" => "onclick",
      "value" => array(__("Open prettyPhoto", "Nimva") => "link_image", __("Do nothing", "Nimva") => "link_no", __("Open custom link", "Nimva") => "custom_link"),
      "description" => __("What to do when slide is clicked?", "Nimva")
    ),
    array(
      "type" => "exploded_textarea",
      "heading" => __("Custom links", "Nimva"),
      "param_name" => "custom_links",
      "description" => __('Enter links for each slide here. Divide links with linebreaks (Enter).', 'Nimva'),
      "dependency" => Array('element' => "onclick", 'value' => array('custom_link'))
    ),
    array(
      "type" => "dropdown",
      "heading" => __("Custom link target", "Nimva"),
      "param_name" => "custom_links_target",
      "description" => __('Select where to open  custom links.', 'Nimva'),
      "dependency" => Array('element' => "onclick", 'value' => array('custom_link')),
      'value' => $target_arr
    ),
	
	$add_css_animation,
	
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  )
) );


/* Tabs
---------------------------------------------------------- */
$tab_id_1 = time().'-1-'.rand(0, 100);
$tab_id_2 = time().'-2-'.rand(0, 100);
vc_map( array(
  "name"  => __("Tabs", "Nimva"),
  "base" => "vc_tabs",
  "show_settings_on_create" => false,
  "is_container" => true,
  "icon" => "icon-wpb-ui-tab-content",
  "category" => __('Content', 'Nimva'),
  "description" => __('Tabbed content', 'Nimva'),
  "params" => array(
	$add_css_animation,
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  ),
  "custom_markup" => '
  <div class="wpb_tabs_holder wpb_holder vc_container_for_children">
  <ul class="tabs_controls">
  </ul>
  %content%
  </div>'
  ,
  'default_content' => '
  [vc_tab title="'.__('Tab 1','Nimva').'" tab_id="'.$tab_id_1.'"][/vc_tab]
  [vc_tab title="'.__('Tab 2','Nimva').'" tab_id="'.$tab_id_2.'"][/vc_tab]
  ',
  "js_view" => ($vc_is_wp_version_3_6_more ? 'VcTabsView' : 'VcTabsView35')
) );

/* Tour section
---------------------------------------------------------- */
$tab_id_1 = time().'-1-'.rand(0, 100);
$tab_id_2 = time().'-2-'.rand(0, 100);
WPBMap::map( 'vc_tour', array(
  "name" => __("Tour Section", "Nimva"),
  "base" => "vc_tour",
  "show_settings_on_create" => false,
  "is_container" => true,
  "container_not_allowed" => true,
  "icon" => "icon-wpb-ui-tab-content-vertical",
  "category" => __('Content', 'Nimva'),
  "wrapper_class" => "clearfix",
  "description" => __('Tabbed tour section', 'Nimva'),
  "params" => array(
    array(
      "type" => "textfield",
      "heading" => __("Widget title", "Nimva"),
      "param_name" => "title",
      "description" => __("What text use as a widget title. Leave blank if no title is needed.", "Nimva")
    ),
	$add_css_animation,
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  ),
  "custom_markup" => '  
  <div class="wpb_tabs_holder wpb_holder clearfix vc_container_for_children">
  <ul class="tabs_controls">
  </ul>
  %content%
  </div>'
  ,
  'default_content' => '
  [vc_tab title="'.__('Slide 1','Nimva').'" tab_id="'.$tab_id_1.'"][/vc_tab]
  [vc_tab title="'.__('Slide 2','Nimva').'" tab_id="'.$tab_id_2.'"][/vc_tab]
  ',
  "js_view" => ($vc_is_wp_version_3_6_more ? 'VcTabsView' : 'VcTabsView35')
) );

vc_map( array(
  "name" => __("Tab", "Nimva"),
  "base" => "vc_tab",
  "allowed_container_element" => 'vc_row',
  "is_container" => true,
  "content_element" => false,
  "params" => array(
    array(
      "type" => "textfield",
      "heading" => __("Title", "Nimva"),
      "param_name" => "title",
      "description" => __("Tab title.", "Nimva")
    ),
	array(
    	"type" => "awesome_icons",
        "heading" => __( "Title Icon", "Nimva" ),
        "param_name" => "icon",
        "width" => 200,
        "value" => $awesome_icons_list,
        "encoding" => "false",
        "description" => __( "Optionally you can insert an icon for the title.", "Nimva" )
    ),
    array(
      "type" => "tab_id",
      "heading" => __("Tab ID", "Nimva"),
      "param_name" => "tab_id"
    )
  ),
  'js_view' => ($vc_is_wp_version_3_6_more ? 'VcTabView' : 'VcTabView35')
) );

/* Toggle (FAQ)
---------------------------------------------------------- */
vc_map( array(
  "name" => __("Toggle", "Nimva"),
  "base" => "vc_toggle",
  "icon" => "icon-wpb-toggle-small-expand",
  "category" => __('Content', 'Nimva'),
  "description" => __('Add a Toggle element', 'Nimva'),
  "params" => array(
    array(
      "type" => "textfield",
      "holder" => "h4",
      "class" => "toggle_title",
      "heading" => __("Toggle title", "Nimva"),
      "param_name" => "title",
      "value" => __("Toggle title", "Nimva"),
      "description" => __("Toggle block title.", "Nimva")
    ),
    array(
      "type" => "textarea_html",
      "holder" => "div",
      "class" => "toggle_content",
      "heading" => __("Toggle content", "Nimva"),
      "param_name" => "content",
      "value" => __("<p>Toggle content goes here, click edit button to change this text.</p>", "Nimva"),
      "description" => __("Toggle block content.", "Nimva")
    ),
	array(
      "type" => "switch",
	  "param_name" => "default_state",
	  "value" => "false",
      "heading" => __("Toggle should be open by default.", "Nimva")      
    ),
	array(
    	"type" => "awesome_icons",
        "heading" => __( "Title Icon", "Nimva" ),
        "param_name" => "icon",
        "width" => 200,
        "value" => $awesome_icons_list,
        "encoding" => "false",
        "description" => __( "Optionally you can insert an icon for the title.", "Nimva" )
    ),
    $add_css_animation,
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  ),
  "js_view" => 'VcToggleView'
) );

/* Accordion block
---------------------------------------------------------- */
vc_map( array(
  "name" => __("Accordion", "Nimva"),
  "base" => "vc_accordion",
  "show_settings_on_create" => false,
  "is_container" => true,
  "icon" => "icon-wpb-ui-accordion",
  "category" => __('Content', 'Nimva'),
  "description" => __('jQuery UI accordion', 'Nimva'),
  "params" => array(
    array(
      "type" => "textfield",
      "heading" => __("Widget title", "Nimva"),
      "param_name" => "title",
      "description" => __("What text use as a widget title. Leave blank if no title is needed.", "Nimva")
    ),
	/*
	array(
      "type" => "dropdown",
      "heading" => __("Accordion Title color", "Nimva"),
      "param_name" => "color",
      "value" => $button_colors,
      "description" => __("Select a color for the accordion title.", "Nimva")
    ),
	
    array(
      "type" => "textfield",
      "heading" => __("Active tab", "Nimva"),
      "param_name" => "active_tab",
      "description" => __("Enter tab number to be active on load or enter false to collapse all tabs.", "Nimva")
    ),
    array(
      "type" => 'checkbox',
      "heading" => __("Allow collapsible all", "Nimva"),
      "param_name" => "collapsible",
      "description" => __("Select checkbox to allow for all sections to be be collapsible.", "Nimva"),
      "value" => Array(__("Allow", "Nimva") => 'yes')
    ),
	*/
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  ),
  "custom_markup" => '
  <div class="wpb_accordion_holder wpb_holder clearfix vc_container_for_children">
  %content%
  </div>
  <div class="tab_controls">
  <button class="add_tab" title="'.__("Add accordion section", "Nimva").'">'.__("Add accordion section", "Nimva").'</button>
  </div>
  ',
  'default_content' => '
  [vc_accordion_tab title="'.__('Section 1', "Nimva").'"][/vc_accordion_tab]
  [vc_accordion_tab title="'.__('Section 2', "Nimva").'"][/vc_accordion_tab]
  ',
  'js_view' => 'VcAccordionView'
) );
vc_map( array(
  "name" => __("Accordion Section", "Nimva"),
  "base" => "vc_accordion_tab",
  "allowed_container_element" => 'vc_row',
  "is_container" => true,
  "content_element" => false,
  "params" => array(
    array(
      "type" => "textfield",
      "heading" => __("Title", "Nimva"),
      "param_name" => "title",
      "description" => __("Accordion section title.", "Nimva")
    ),
	array(
    	"type" => "awesome_icons",
        "heading" => __( "Title Icon", "Nimva" ),
        "param_name" => "icon",
        "width" => 200,
        "value" => $awesome_icons_list,
        "encoding" => "false",
        "description" => __( "Optionally you can insert an icon for the title.", "Nimva" )
    ),
	
  ),
  'js_view' => 'VcAccordionTabView'
) );

/* Posts Grid
---------------------------------------------------------- */

vc_map( array(
  "name" => __("Posts Grid", "Nimva"),
  "base" => "recent_posts",
  "icon" => "icon-wpb-application-icon-large",
  "category" => __('Content', 'Nimva'),
  "description" => __('Posts in grid view', 'Nimva'),
  "params" => array(
  	array(
      "type" => "textfield",
      "heading" => __("Box Title", "Nimva"),
	  "admin_label" => true,
      "param_name" => "box_title",
      "description" => __("Enter a title for the entire recent posts section.", "Nimva")
    ),
	array(
    	"type" => "range",
        "heading" => __( "Posts count", "Nimva" ),
        "param_name" => "items",
        "value" => "6",
        "min" => "1",
		"read" => "readonly",
		"admin_label" => true,
        "max" => "50",
        "step" => "1",
        "unit" => 'px',
        "description" => __( "Select the number of posts you want to be displayed.", "Nimva" )
    ),
	
	array(
      "type" => "dropdown",
      "heading" => __("Style", "Nimva"),
      "param_name" => "style",
	  "admin_label" => true,
      "value" => array(__("Style 1", "Nimva") => "style1", __("Style 2", "Nimva") => "style2"),
      "description" => __("Select the way the posts will be displayed.", "Nimva")
    ),
	
	array(
    	"type" => "range",
        "heading" => __( "Posts columns", "Nimva" ),
        "param_name" => "columns",
        "value" => "3",
		"read" => "readonly",
		"admin_label" => true,
        "min" => "3",
        "max" => "5",
        "step" => "1",
        "unit" => 'px',
		"dependency" => Array('element' => "style", 'value' => "style1"),
        "description" => __( "Select the number of posts you want to be displayed.", "Nimva" )
    ),
	
	array(
      "type" => "switch",
      "heading" => __("Show Post Title", "Nimva"),
      "param_name" => "posts_title",
	  "admin_label" => true,
      "value" => "true",
      "description" => __("Show/hide post title.", "Nimva")
    ),

	array(
      "type" => "switch",
      "heading" => __("Show Thumbnail", "Nimva"),
      "param_name" => "thumbnail",
	  "admin_label" => true,
      "value" => "true",
      "description" => __("Show/hide post thumbnail.", "Nimva")
    ),
	
	array(
      "type" => "switch",
      "heading" => __("Show Date", "Nimva"),
      "param_name" => "show_date",
	  "admin_label" => true,
      "value" => "true",
      "description" => __("Show/hide post date.", "Nimva")
    ),

	array(
      "type" => "switch",
      "heading" => __("Show Excerpt", "Nimva"),
      "param_name" => "show_excerpt",
	  "admin_label" => true,
      "value" => "true",
      "description" => __("Show/hide post excerpt.", "Nimva")
    ),
	
	array(
      "type" => "switch",
      "heading" => __("Show Continue Reading Link", "Nimva"),
      "param_name" => "continue_reading",
	  "admin_label" => true,
      "value" => "true",
      "description" => __("Show/hide \"continue reading\" link to the actual post page.", "Nimva")
    ),
	array(
    	"type" => "multiselect",
        "heading" => __( "Select specific Posts", "Nimva" ),
        "param_name" => "posts",
        "options" => $posts,
        "value" => '',
	),
	array(
    	"type" => "multiselect",
        "heading" => __( "Select specific Categories", "Nimva" ),
        "param_name" => "category_id",
        "options" => $categories,
        "value" => ''
    ),
	
	array(
      "type" => "dropdown",
      "heading" => __("Order Posts by date", "Nimva"),
      "param_name" => "posts_order",
	  "admin_label" => true,
      "value" => array( __("Descending", "Nimva") => "DESC", __("Ascending", "Nimva") => "ASC"),
      "description" => __("Select the way the posts will be ordered - ascending or descending.", "Nimva")
    ),
	
	$add_css_animation,
	
	array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  )
) );

/* Portfolio items
---------------------------------------------------------- */

vc_map( array(
  "name" => __("Portfolio", "Nimva"),
  "base" => "recent_works",
  "icon" => "icon-wpb-portfolio",
  "category" => __('Content', 'Nimva'),
  "description" => __('Add portfolio items', 'Nimva'),
  "params" => array(
  	array(
      "type" => "textfield",
      "heading" => __("Box Title", "Nimva"),
      "param_name" => "title",
	  "admin_label" => true,
      "description" => __("You should enter a title if you are planning to use the Carousel style.", "Nimva")
    ),
  	array(
    	"type" => "range",
        "heading" => __( "Items count", "Nimva" ),
        "param_name" => "items",
        "value" => "6",
        "min" => "1",
		"read" => "readonly",
		"admin_label" => true,
        "max" => "50",
        "step" => "1",
        "unit" => 'px',
        "description" => __( "Select the number of portfolio items you want to be displayed.", "Nimva" )
    ),
	array(
    	"type" => "multiselect",
        "heading" => __( "Select specific Portfolio items", "Nimva" ),
        "param_name" => "posts",
        "options" => $portfolio_posts,
        "value" => ''
    ),
	array(
    	"type" => "multiselect",
        "heading" => __( "Select Categories", "Nimva" ),
        "param_name" => "category",
        "options" => $taxon,
        "value" => '',
        "description" => __( "Only show portfolio items from these categories", "Nimva" )
    ),
	
	array(
      "type" => "switch",
      "heading" => __("Show Portfolio Details", "Nimva"),
      "param_name" => "portfolio_details",	  
      "value" => "true",
      "description" => __("Show/hide portfolio item details - title &amp; category below image.", "Nimva")
    ),
	
	array(
      "type" => "dropdown",
      "heading" => __("Portfolio Display", "Nimva"),
      "param_name" => "portfolio_display",
	  "admin_label" => true,
      "value" => array(__("Carousel", "Nimva") => "carousel", __("Normal", "Nimva") => "normal"),
      "description" => __("Select the way the portfolio items will be displayed.", "Nimva")
    ),
	
	array(
      "type" => "switch",
      "heading" => __("Enable Fitlters", "Nimva"),
      "param_name" => "show_filters",
	  "dependency" => Array('element' => "portfolio_display", 'value' => "normal"),
	  "admin_label" => true,
      "value" => "true",
      "description" => __("Show/hide portfolio filters.", "Nimva")
    ),
	
	array(
      "type" => "dropdown",
      "heading" => __("Filters position", "Nimva"),
      "param_name" => "filter_pos",
	  "dependency" => Array('element' => "portfolio_display", 'value' => "normal"),
	  "admin_label" => true,
      "value" => array(__("Left", "Nimva") => "left", __("Center", "Nimva") => "center", __("Right", "Nimva") => "right"),
      "description" => __("Select the way the portfolio items will be displayed.", "Nimva")
    ),
	
	array(
    	"type" => "range",
        "heading" => __( "Columns", "Nimva" ),
        "param_name" => "columns",
        "value" => "4",
        "min" => "2",
		"read" => "readonly",
		"admin_label" => true,
		"dependency" => Array('element' => "portfolio_display", 'value' => "normal"),
        "max" => "4",
        "step" => "1",
        "unit" => 'columns',
        "description" => __( "Select the number of columns used to display portfolio items.", "Nimva" )
    ),
	
	$add_css_animation,
	
	array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )

  )
) );


/* Featured Services
---------------------------------------------------------- */
vc_map( array(
  "name" => __("Featured Services", "Nimva"),
  "base" => "vc_service_box",
  "icon" => "icon-wpb-service-box",
  "category" => __('Content', 'Nimva'),
  "description" => __('Add a featured services box', 'Nimva'),
  "params" => array(
  
  	array(
      "type" => "attach_image",
      "heading" => __("Image Icon", "Nimva"),
      "param_name" => "icon",
      "value" => "",
      "description" => __("Select/upload an image icon from media library. Leave blank if you want to use an Icon bellow.", "Nimva")
    ),
	
	array(
    	"type" => "awesome_icons",
        "heading" => __( "Text Icon", "Nimva" ),
        "param_name" => "text_icon",		
        "width" => 200,
        "value" => $awesome_icons_list,
        "encoding" => "false",
		"holder" => "div",
        "description" => __( "Add a font icon to your Featured Services box.", "Nimva" )
    ),
	array(
		  "type" => "colorpicker",
		  "heading" => __("Icon color", 'Nimva'),
		  "param_name" => "icon_color",
		  "value" => '',
		  "description" => __("Select the color of the icon", 'Nimva')
		),
    	
    array(
      "type" => "textfield",
      "heading" => __("Heading Title", "Nimva"),
      "param_name" => "title",
	  "holder" => "h2",
      "value" => __("Design", "Nimva"),
      "description" => __("Text on the button.", "Nimva")
    ),
	
	array(
		  "type" => "colorpicker",
		  "heading" => __("Title color", 'Nimva'),
		  "param_name" => "title_color",
		  "value" => '',
		  "description" => __("Select the color of the title - if a link is set this option will be ignored.", 'Nimva')
		),
	
    array(
      "type" => "textfield",
      "heading" => __("URL (Link)", "Nimva"),
      "param_name" => "href",
      "description" => __("Button link.", "Nimva")
    ),
	
    array(
      "type" => "dropdown",
      "heading" => __("Target", "Nimva"),
      "param_name" => "target",
      "value" => $target_arr,
      "dependency" => Array('element' => "href", 'not_empty' => true)
    ),
	/*
	array(
			"type" => "switch",
			"heading" => __("Make entire box linkable", "Nimva"),
			"param_name" => "full_link",
			"value" => "false",
			"description" => __( "Make the entire Featured Service box linkable.", "Nimva" )
	),
	*/
	array(
      "type" => "textarea_html",
      "holder" => "div",
      "heading" => __("Description Text", "Nimva"),
      "param_name" => "content",
      "value" => __("<p>I am text block. Click edit button to change this text.</p>", "Nimva")
    ),
	
	array(
		  "type" => "colorpicker",
		  "heading" => __("Text color", 'Nimva'),
		  "param_name" => "text_color",
		  "value" => '',
		  "description" => __("Select the color of the description text.", 'Nimva')
		),
		
	array(
		  "type" => "colorpicker",
		  "heading" => __("Background color", 'Nimva'),
		  "param_name" => "bg_color",
		  "value" => '',
		  "description" => __("Select the color of the background.", 'Nimva')
		),	
		
	array(
			"type" => "range",
			"heading" => __( "Background Opacity", "Nimva" ),
			"param_name" => "bg_opacity",
			"description" => __('Select the background opacity of the featured serives box - 0 will make the box background transparent.', 'Nimva'),
			"value" => "100",
			"min" => "0",
			"max" => "100",
			"step" => "5",
			"unit" => 'pixels'			
		),	
	
	$add_css_animation,
	
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  ),
  "js_view" => 'VcServices'
) );

/**
 * Product Feature 
 */

 
vc_map( array(
    "name" => __("Product Feature", "Nimva"),
    "base" => "product_feature",
    "icon" => "icon-wpb-product-feature",
    "category" => __('Content', 'Nimva'),
	"description" => __('Add a product feature.', 'Nimva'),
	"wrapper_class" => "clearfix",
    "params" => array(	
		
		array(
            "type" => "textfield",
            "heading" => __("Product Feature title", "Nimva"),
            "param_name" => "title",
			"holder" => "h2",
            "description" => __("Add the name of the Product Feature", "Nimva")
        ),
		
		array(
		  "type" => "colorpicker",
		  "heading" => __("Title color", 'Nimva'),
		  "param_name" => "title_color",
		  "value" => '',
		  "description" => __("Select the color of the title", 'Nimva')
		),
		
		array(
			"type" => "range",
			"heading" => __( "Title font size", "Nimva" ),
			"param_name" => "title_font_size",
			"description" => __('Select the font size for the title', 'Nimva'),
			"value" => "20",
			"min" => "10",
			"max" => "35",
			"step" => "1",
			"unit" => 'pixels'			
		),
		
		array(
			"type" => "dropdown",
			"heading" => __("Font Weight", "Nimva"),
			"param_name" => "font_weight",
			"value" => array(__("Normal", "Nimva") => "normal", __("Bold", "Nimva") => "bold"),
			"description" => __( "Select the font weight", "Nimva" )
		),
		
		array(
            "type" => "textarea",
            "heading" => __("Product Feature description", "Nimva"),
            "param_name" => "content",
			"holder" => "div",
            "description" => __("Add a description for this Product Feature", "Nimva")
        ),
		
		array(
		  "type" => "colorpicker",
		  "heading" => __("Description color", 'Nimva'),
		  "param_name" => "description_color",
		  "value" => '#777777',
		  "description" => __("Select the color of the description", 'Nimva')
		),	
		
		array(
			"type" => "range",
			"heading" => __( "Description font size", "Nimva" ),
			"param_name" => "description_font_size",
			"description" => __('Select the font size for the description', 'Nimva'),
			"value" => "12",
			"min" => "10",
			"max" => "35",
			"step" => "1",
			"unit" => 'pixels'			
		),

		array(
			"type" => "dropdown",
			"heading" => __("Style", "Nimva"),
			"param_name" => "style",
			"value" => array(__("Style 1", "Nimva") => "1", __("Style 2", "Nimva") => "2"),
			"description" => __( "Select the style you want to render for this Product Feature", "Nimva" )
		),
		
		array(
			"type" => "dropdown",
			"heading" => __("Elements Orientation", "Nimva"),
			"param_name" => "orientation",
			"value" => array(__("To Left", "Nimva") => "toleft", __("To Right", "Nimva") => "toright"),
			"description" => __( "Select the orientation of the elements inside the Product Feature element.", "Nimva" ),
			"dependency" => Array('element' => 'style', 'value' => array('1'))
		),
		
		array(
		  "type" => "attach_image",
		  "heading" => __("Image Icon", "Nimva"),
		  "param_name" => "picture_icon_url",
		  "value" => "",
		  "description" => __("Select/upload an image icon from media library. Leave blank if you want to use an Icon bellow.", "Nimva")
		),
		
		array(
			"type" => "awesome_icons",
			"heading" => __( "Product Feature Icon", "Nimva" ),
			"param_name" => "icon",
			"width" => 200,
			"value" => $awesome_icons_list,
			"holder" => "div",
			"encoding" => "false"
		),
		
		array(
		  "type" => "colorpicker",
		  "heading" => __("Icon color", 'Nimva'),
		  "param_name" => "icon_color",
		  "value" => '',
		  "description" => __("Select the color of the icon", 'Nimva')
		),
		
		array(
		  "type" => "colorpicker",
		  "heading" => __("Icon background color", 'Nimva'),
		  "param_name" => "icon_bg_color",
		  "value" => '',
		  "description" => __("Select the background color of the icon", 'Nimva')
		),
		
		array(
			"type" => "range",
			"heading" => __( "Icon Size", "Nimva" ),
			"param_name" => "icon_size",
			"description" => __('Select the size of the icon', 'Nimva'),
			"value" => "20",
			"min" => "10",
			"max" => "50",
			"step" => "1",
			"unit" => 'pixels',			
		),
		
       array(
		  "type" => "textfield",
		  "heading" => __("URL (Link)", "Nimva"),
		  "param_name" => "link",
		  "description" => __("Add a link to your Product Feature.", "Nimva")
		),
		
		array(
		  "type" => "dropdown",
		  "heading" => __("Target", "Nimva"),
		  "param_name" => "target",
		  "value" => $target_arr,
		  "dependency" => Array('element' => "link", 'not_empty' => true)
		),	
		
		$add_css_animation,
			
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "Nimva"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
        ),

    )
	,
	"js_view" => 'VcProductFeature'
) );

/**
 * Faq 
 */
 
vc_map( array(
    "name" => __("FAQs", "Nimva"),
    "base" => "faqs",
    "class" => "",
    "icon" => "icon-wpb-faq",
    "category" => __('Content', 'Nimva'),
	"description" => __('Add FAQs to your site.', 'Nimva'),
    "params" => array(	
		/*
		array(
            "type" => "textfield",
            "heading" => __("ID", "Nimva"),
            "param_name" => "faq_id",
            "description" => __("Add a unique ID identifier (name or number) if you are using more than one FAQs section on a page", "Nimva")
        ),
		*/
        array(
			"type" => "range",
			"heading" => __( "Count", "Nimva" ),
			"param_name" => "count",
			"description" => __('How many FAQs you would like to show? (-1 means unlimited)', 'Nimva'),
			"value" => "10",
			"min" => "-1",
			"max" => "50",
			"step" => "1",
			"unit" => 'clients',
			"admin_label" => true			
		),
		
		array(
		  "type" => "switch",
		  "param_name" => "filters",
		  "value" => "true",
		  "heading" => __("Show/hide filters.", "Nimva"),
		),
		array(
			"type" => "multiselect",
			"heading" => __( "Select Categories", "Nimva" ),
			"param_name" => "category",
			"options" => $taxon2,
			"value" => '',
			"description" => __( "Only show FAQs items from these categories", "Nimva" )
		),
		
        array(
            "type" => "multiselect",
            "heading" => __( "Select specific FAQs", "Nimva" ),
            "param_name" => "faqs",
            "value" => '',
            "options" => $faq,
            "description" => ""
        ),		
		
		$add_css_animation,
		
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "Nimva"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
        ),

    )
) );

/* Video element
---------------------------------------------------------- */
vc_map( array(
  "name" => __("Video Player", "Nimva"),
  "base" => "vc_video",
  "icon" => "icon-wpb-film-youtube",
  "category" => __('Content', 'Nimva'),
  "description" => __('Embed YouTube/Vimeo player', 'Nimva'),
  "params" => array(
    array(
      "type" => "textfield",
      "heading" => __("Widget title", "Nimva"),
      "param_name" => "title",
      "description" => __("What text use as a widget title. Leave blank if no title is needed.", "Nimva")
    ),
    array(
      "type" => "textfield",
      "heading" => __("Video link", "Nimva"),
      "param_name" => "link",
      "admin_label" => true,
      "description" => sprintf(__('Link to the video. More about supported formats at %s.', "Nimva"), '<a href="http://codex.wordpress.org/Embeds#Okay.2C_So_What_Sites_Can_I_Embed_From.3F" target="_blank">WordPress codex page</a>')
    ),
	
	$add_css_animation,
	
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  )
) );

/* Google maps element
---------------------------------------------------------- */
vc_map( array(
  "name" => __("Google Maps", "Nimva"),
  "base" => "vc_gmaps",
  "icon" => "icon-wpb-map-pin",
  "category" => __('Content', 'Nimva'),
  "description" => __('Map block', 'Nimva'),
  "params" => array(
  	
    array(
      "type" => "textfield",
      "heading" => __("Google Map Popup Title", "Nimva"),
      "param_name" => "title",
      "description" => __("Example: We Are RockyThemes.", "Nimva")
    ),
	array(
      "type" => "textarea_safe",
      "heading" => __("Google Map Popup Short Message", "Nimva"),
      "param_name" => "message",
      "description" => __("Enter a short message to show in the Popup Marker. Leave blank if you don't have any.", "Nimva")
    ),
/*
    array(
      "type" => "textfield",
      "heading" => __("Google map link", "Nimva"),
      "param_name" => "link",
      "admin_label" => true,
      "description" => sprintf(__('Link to your map. Visit %s find your address and then click "Link" button to obtain your map link.', "Nimva"), '<a href="http://maps.google.com" target="_blank">Google maps</a>')
    ),
	*/
	array(
			'type' => 'textfield',
			'heading' => __( 'Address', 'Nimva' ),
			'param_name' => 'address',
			'description' => __( 'Enter here your map address. Example: New York Ave, Brooklyn, New York', 'Nimva' ),
	),		

	
	array(
			"type" => "range",
			"heading" => __( "Map Height", "Nimva" ),
			"param_name" => "size",
			"description" => __('Enter map height in pixels', 'Nimva'),
			"value" => "250",
			"min" => "50",
			"max" => "600",
			"step" => "10",
			"unit" => 'pixels'		
		),
	
    array(
      "type" => "dropdown",
      "heading" => __("Map type", "Nimva"),
      "param_name" => "type",
      "value" => array(__("Roadmap", "Nimva") => "ROADMAP", __("Satellite", "Nimva") => "SATELLITE", __("Hybrid", "Nimva") => "HYBRID", __("Terrain", "Nimva") => "TERRAIN"),
      "description" => __("Select map type.", "Nimva")
    ),
	
    array(
      "type" => "dropdown",
      "heading" => __("Map Zoom", "Nimva"),
      "param_name" => "zoom",
      "value" => array(__("14 - Default", "Nimva") => 14, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 15, 16, 17, 18, 19, 20)
    ),
	/*
    array(
      "type" => 'checkbox',
      "heading" => __("Remove info bubble", "Nimva"),
      "param_name" => "bubble",
      "description" => __("If selected, information bubble will be hidden.", "Nimva"),
      "value" => Array(__("Yes, please", "Nimva") => true),
    ),
	*/
	
	array(
      "type" => "textfield",
      "heading" => __("Google Map Email Address", "Nimva"),
      "param_name" => "email",
      "description" => __("Enter your email address if you want to display it on the google map.", "Nimva")
    ),
	
	array(
      "type" => "textfield",
      "heading" => __("Google Map Phone Number", "Nimva"),
      "param_name" => "phone",
      "description" => __("Enter your phone number if you want to display it on the google map.", "Nimva")
    ),
	
	array(
		"type" => "switch",
		"heading" => __("Show Popup on page load", "Nimva"),
		"param_name" => "popup",
		"value" => "true",
	),
	
	array(
		"type" => "switch",
		"heading" => __("Map Scrollwheel", "Nimva"),
		"param_name" => "scrollwheel",
		"value" => "true",
	),
	
	array(
		"type" => "switch",
		"heading" => __("Map Pan Controls", "Nimva"),
		"param_name" => "pan",
		"value" => "true",
	),
	
	array(
		"type" => "switch",
		"heading" => __("Map Zoom Controls", "Nimva"),
		"param_name" => "zoom_control",
		"value" => "true",
	),
	
	array(
		"type" => "switch",
		"heading" => __("Map Type Controls", "Nimva"),
		"param_name" => "type_control",
		"value" => "true",
	),	
	
	array(
		"type" => "switch",
		"heading" => __("Map Street View Controls", "Nimva"),
		"param_name" => "streetview",
		"value" => "true",
	),
	
	
	$add_css_animation,
	
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  )
) );

/* Raw HTML
---------------------------------------------------------- */
vc_map( array(
  "name" => __("Raw HTML", "Nimva"),
	"base" => "vc_raw_html",
	"icon" => "icon-wpb-raw-html",
	"category" => __('Structure', 'Nimva'),
	"wrapper_class" => "clearfix",
	"description" => __('Output raw html code on your page', 'Nimva'),
	"params" => array(
		array(
  		"type" => "textarea_raw_html",
			"holder" => "div",
			"heading" => __("Raw HTML", "Nimva"),
			"param_name" => "content",
			"value" => base64_encode("<p>I am raw html block.<br/>Click edit button to change this html</p>"),
			"description" => __("Enter your HTML content.", "Nimva")
		),
	)
) );

/* Raw JS
---------------------------------------------------------- */
vc_map( array(
	"name" => __("Raw JS", "Nimva"),
	"base" => "vc_raw_js",
	"icon" => "icon-wpb-raw-javascript",
	"category" => __('Structure', 'Nimva'),
	"wrapper_class" => "clearfix",
	"description" => __('Output raw javascript code on your page', 'Nimva'),
	"params" => array(
  	array(
  		"type" => "textarea_raw_html",
			"holder" => "div",
			"heading" => __("Raw js", "Nimva"),
			"param_name" => "content",
			"value" => __(base64_encode("<script type='text/javascript'> alert('Enter your js here!'); </script>"), "Nimva"),
			"description" => __("Enter your JS code.", "Nimva")
		),
	)
) );



/**
 * Social Links 
 */
 
vc_map( array(
    "name" => __("Social Links", "Nimva"),
    "base" => "social_links",
    "class" => "",
    "icon" => "icon-wpb-social-links",
    "category" => __('Content', 'Nimva'),
	"description" => __('Add social links', 'Nimva'),
    "params" => array(
	
		array(
			"type" => "dropdown",
			"heading" => __("Position", "Nimva"),
			"param_name" => "position",
			"value" => array(__("Left", "Nimva") => "left", __("Center", "Nimva") => "center", __("Right", "Nimva") => "right"),
			"description" => __( "Select the alignment of the social links.", "Nimva" )
		),
		/*
		array(
			"type" => "switch",
			"heading" => __("Transparent Background", "Nimva"),
			"param_name" => "transparent",
			"value" => "false",
			"description" => __( "Make the background of the Social Icons transparent.", "Nimva" )
		),	
		*/
		
		array(
		  "type" => "colorpicker",
		  "heading" => __("Social Links Color", 'Nimva'),
		  "param_name" => "color",
		  "value" => '',
		  "description" => __("Select the color of the Social Links.", 'Nimva')
		),
		
		array(
            "type" => "textfield",
            "heading" => __( "Facebook", "Nimva" ),
            "param_name" => "facebook",
            "value" => "",
            "description" => __( "Provide a link for your Facebook profile.", "Nimva" ),
			"admin_label" => true
        ),
		
		array(
            "type" => "textfield",
            "heading" => __( "Twitter", "Nimva" ),
            "param_name" => "twitter",
            "value" => "",
            "description" => __( "Provide a link for your Twitter profile.", "Nimva" ),
			"admin_label" => true
        ),
		
		array(
            "type" => "textfield",
            "heading" => __( "Linkedin", "Nimva" ),
            "param_name" => "linkedin",
            "value" => "",
            "description" => __( "Provide a link for your LinkedIn profile.", "Nimva" ),
			"admin_label" => true
        ),
		
		array(
            "type" => "textfield",
            "heading" => __( "Google+", "Nimva" ),
            "param_name" => "google",
            "value" => "",
            "description" => __( "Provide a link for your Google+ profile.", "Nimva" ),
			"admin_label" => true
        ),
		
		array(
            "type" => "textfield",
            "heading" => __( "Instagram", "Nimva" ),
            "param_name" => "instagram",
            "value" => "",
            "description" => __( "Provide a link for your Instagram profile.", "Nimva" ),
			"admin_label" => true
        ),
		
		array(
            "type" => "textfield",
            "heading" => __( "Pinteres", "Nimva" ),
            "param_name" => "pinterest",
            "value" => "",
            "description" => __( "Provide a link for your Pinterest profile.", "Nimva" ),
			"admin_label" => true
        ),
		
		array(
            "type" => "textfield",
            "heading" => __( "Flickr", "Nimva" ),
            "param_name" => "flickr",
            "value" => "",
            "description" => __( "Provide a link for your Flickr profile.", "Nimva" ),
			"admin_label" => true
        ),
		
		array(
            "type" => "textfield",
            "heading" => __( "Dribbble", "Nimva" ),
            "param_name" => "dribbble",
            "value" => "",
            "description" => __( "Provide a link for your Dribbble profile.", "Nimva" ),
			"admin_label" => true
        ),

        array(
            "type" => "textfield",
            "heading" => __( "Vimeo", "Nimva" ),
            "param_name" => "vimeo",
            "value" => "",
            "description" => __( "Provide a link for your Vimeo profile.", "Nimva" ),
			"admin_label" => true
        ),
		
		array(
            "type" => "textfield",
            "heading" => __( "Youtube", "Nimva" ),
            "param_name" => "youtube",
            "value" => "",
            "description" => __( "Provide a link for your youtube profile.", "Nimva" ),
			"admin_label" => true
        ),
		
		array(
            "type" => "textfield",
            "heading" => __( "Skype", "Nimva" ),
            "param_name" => "skype",
            "value" => "",
            "description" => __( "Provide a link for your Skype profile.", "Nimva" ),
			"admin_label" => true
        ),
		
		array(
            "type" => "textfield",
            "heading" => __( "Behance", "Nimva" ),
            "param_name" => "behance",
            "value" => "",
            "description" => __( "Provide a link for your Behance profile.", "Nimva" ),
			"admin_label" => true
        ),
		
		array(
            "type" => "textfield",
            "heading" => __( "Tumblr", "Nimva" ),
            "param_name" => "tumblr",
            "value" => "",
            "description" => __( "Provide a link for your Tumblr profile.", "Nimva" ),
			"admin_label" => true
        ),
		
		$add_css_animation,
		
		array(
            "type" => "textfield",
            "heading" => __("Extra class name", "Nimva"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
        ),
    )
) );

/**
 * Clients 
 */
 
vc_map( array(
    "name" => __("Clients", "Nimva"),
    "base" => "clients",
    "class" => "",
    "icon" => "icon-wpb-clients",
    "category" => __('Content', 'Nimva'),
	"description" => __('Add clients to your site.', 'Nimva'),
    "params" => array(
		
		array(
            "type" => "textfield",
            "heading" => __( "Heading Title", "Nimva" ),
            "param_name" => "title",
            "value" => "",
            "description" => __( "Provide a title for your clients section", "Nimva" ),
			"admin_label" => true
        ),

        array(
			"type" => "range",
			"heading" => __( "Count", "Nimva" ),
			"param_name" => "count",
			"description" => __('How many Clients you would like to show? (-1 means unlimited)', 'Nimva'),
			"value" => "10",
			"min" => "-1",
			"max" => "50",
			"step" => "1",
			"unit" => 'clients',
			"admin_label" => true			
		),
        array(
            "type" => "multiselect",
            "heading" => __( "Select specific Clients", "Nimva" ),
            "param_name" => "clients",
            "value" => '',
            "options" => $clients,
            "description" => ""
        ),	
		
		$add_css_animation,
			
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "Nimva"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
        ),

    )
) );

/**
 * Testimonials 
 */
 
vc_map( array(
    "name" => __("Testimonials", "Nimva"),
    "base" => "testimonials",
    "class" => "",
    "icon" => "icon-wpb-testimonials",
    "category" => __('Content', 'Nimva'),
	"description" => __('Add testimonials to your site', 'Nimva'),
    "params" => array(
		
		array(
            "type" => "textfield",
            "heading" => __( "Testimonials Section Title", "Nimva" ),
            "param_name" => "title",
            "value" => "",
            "description" => __( "Provide a title for your testimonial section", "Nimva" ),
			"admin_label" => true
        ),

        array(
			"type" => "range",
			"heading" => __( "Count", "Nimva" ),
			"param_name" => "count",
			"description" => __('How many testimonials you would like to show? (-1 means unlimited)', 'Nimva'),
			"value" => "10",
			"min" => "-1",
			"max" => "50",
			"step" => "1",
			"unit" => 'testimonials',
			"admin_label" => true			
		),
        array(
            "type" => "multiselect",
            "heading" => __( "Select specific Testimonials", "Nimva" ),
            "param_name" => "testimonials",
            "value" => '',
            "options" => $testimonials,
            "description" => ""
        ),	
		
		array(
			"type" => "dropdown",
			"heading" => __("Testimonials Display", "Nimva"),
			"param_name" => "display",
			"value" => array(__("Slideshow", "Nimva") => "slideshow", __("Columns", "Nimva") => "columns"),
			"description" => __( "Select how you want the testimonials to be displayed.", "Nimva" )
		),
		
		array(
			"type" => "dropdown",
			"heading" => __("Navigation position", "Nimva"),
			"param_name" => "navigation",
			"value" => array(__("Top Right", "Nimva") => "topr", __("Centered", "Nimva") => "center"),
			"description" => __( "Select the alignment of the navigation keys.", "Nimva" ),
			"dependency" => Array('element' => 'display', 'value' => array('slideshow'))
		),
		
		
		array(
		  "type" => "range",
		  "heading" => __("Columns Count", "Nimva"),
		  "param_name" => "columns_count",
		  "description" => __('How many columns to use?', 'Nimva'),
		  "value" => "3",
		  "min" => "1",
		  "max" => "4",
		  "step" => "1",
		  "unit" => 'columns',		  
		  "dependency" => Array('element' => 'display', 'value' => array('columns')),
		),
		
		array(
			"type" => "dropdown",
			"heading" => __("Text align", "Nimva"),
			"param_name" => "align",
			"value" => array(__("Left", "Nimva") => "left", __("Right", "Nimva") => "right", __("Center", "Nimva") => "center"),
			"description" => __( "Select the alignment of the text.", "Nimva" )
		),
		
		array(
		  "type" => "colorpicker",
		  "heading" => __("Text color", 'Nimva'),
		  "param_name" => "color",
		  "value" => '',
		  "description" => __("Select the color of the text. Leave blank to use the color defined in Theme Options -> Shortcodes -> Testimonial", 'Nimva')
		),
		
		array(
		  "type" => "colorpicker",
		  "heading" => __("Background color", 'Nimva'),
		  "param_name" => "background_color",
		  "value" => '',
		  "description" => __("Select the color of the background. Leave blank to use the color defined in Theme Options -> Shortcodes -> Testimonial", 'Nimva')
		),
		
		array(
		  "type" => "colorpicker",
		  "heading" => __("Border color", 'Nimva'),
		  "param_name" => "border_color",
		  "value" => '',
		  "description" => __("Select the color of the border of the testimonial box. Leave blank to use the color defined in Theme Options -> Shortcodes -> Testimonial", 'Nimva')
		),
		
		$add_css_animation,
			
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "Nimva"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
        ),

    )
) );

/**
 * Pricing Table 
 */
 
vc_map( array(
    "name" => __("Pricing Table", "Nimva"),
    "base" => "pricing_table",
    "class" => "",
    "icon" => "icon-wpb-pricing-table",
    "category" => __('Content', 'Nimva'),
	"description" => __('Add clients to your site.', 'Nimva'),
    "params" => array(		

        array(
			"type" => "range",
			"heading" => __( "Pricing Table Columns", "Nimva" ),
			"param_name" => "columns",
			"description" => __('Select how many columns to use for this pricing table.', 'Nimva'),
			"value" => "4",
			"min" => "3",
			"max" => "5",
			"step" => "1",
			"unit" => 'columns',
			"read" => "readonly",
			"admin_label" => true			
		),
        array(
			"type" => "switch",
			"heading" => __("Spaced Columns", "Nimva"),
			"param_name" => "spaced_columns",
			"value" => "false",
			"description" => __( "Add spacing between the pricing table columns", "Nimva" )
		),	
		array(
            "type" => "multiselect",
            "heading" => __( "Select specific columns to create a Pricing Table", "Nimva" ),
            "param_name" => "specific_columns",
            "value" => '',
            "options" => $pricings,
            "description" => ""
        ),	
		
		$add_css_animation,
		
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "Nimva"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
        ),

    )
) );

/**
 * Employee 
 */
 
vc_map( array(
    "name" => __("Employee", "Nimva"),
    "base" => "employee",
    "class" => "",
    "icon" => "icon-wpb-employee",
    "category" => __('Content', 'Nimva'),
	"description" => __('Add Employees profiles', 'Nimva'),
    "params" => array(
		
		array(
			"type" => "range",
			"heading" => __( "Columns", "Nimva" ),
			"param_name" => "columns",
			"description" => __('How many columns to use to display employees?', 'Nimva'),
			"value" => "4",
			"min" => "1",
			"max" => "4",
			"step" => "1",
			"unit" => 'columns',
			"admin_label" => true			
		),

        array(
			"type" => "range",
			"heading" => __( "Employees Count", "Nimva" ),
			"param_name" => "count",
			"description" => __('How many Employees would you like to show? (-1 means unlimited)', 'Nimva'),
			"value" => "10",
			"min" => "-1",
			"max" => "50",
			"step" => "1",
			"unit" => 'employees',
			"admin_label" => true			
		),
        array(
            "type" => "multiselect",
            "heading" => __( "Select specific Employees", "Nimva" ),
            "param_name" => "employees",
            "value" => '',
            "options" => $employees,
            "description" => ""
        ),	
		array(
		  "type" => "switch",
		  "param_name" => "bio",
		  "value" => "true",
		  "heading" => __("Show/hide employees \"About\" description.", "Nimva"),
		),	
		array(
		  "type" => "switch",
		  "param_name" => "profiles",
		  "value" => "true",
		  "heading" => __("Show/hide employees social networking profiles.", "Nimva"),
		),
		
		array(
		  "type" => "colorpicker",
		  "heading" => __("Social Newtorks color", "Nimva"),
		  "param_name" => "color",
		  "description" => __("Select a custom color for the social network profiles. ", "Nimva"),		  
		),
		
		$add_css_animation,
		
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "Nimva"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
        ),

    )
) );





/* Graph
---------------------------------------------------------- */
vc_map( array(
  "name" => __("Progress Bar", "Nimva"),
  "base" => "vc_progress_bar",
  "icon" => "icon-wpb-graph",
  "category" => __('Content', 'Nimva'),
  "description" => __('Animated progress bar', 'Nimva'),
  "params" => array(

    array(
      "type" => "exploded_textarea",
      "heading" => __("Graphic values", "Nimva"),
      "param_name" => "values",
      "description" => __('Input graph values here. Divide values with linebreaks (Enter). Example: 90|Development', 'Nimva'),
      "value" => "90|Development,80|Design,70|Marketing"
    ),
    array(
      "type" => "textfield",
      "heading" => __("Units", "Nimva"),
      "param_name" => "units",
      "description" => __("Enter measurement units (if needed) Eg. %, px, points, etc. ", "Nimva")
    ),
    array(
      "type" => "dropdown",
      "heading" => __("Styles", "Nimva"),
      "param_name" => "style",
      "value" => array(__("Default", "Nimva") => "default", __("Strippes", "Nimva") => "strippes", __("Animated Strippes", "Nimva") => "animated_strippes"),
      "description" => __("Select a style for the progress bar.", "Nimva"),
      "admin_label" => true
    ),
    array(
      "type" => "colorpicker",
      "heading" => __("Bar custom color", "Nimva"),
      "param_name" => "custombgcolor",
      "description" => __("Select custom background color for the progress bars. Leave empty to use the default background color set in Theme Options.", "Nimva"),
      "dependency" => Array('element' => "bgcolor", 'value' => array('custom'))
    ),
	array(
      "type" => "colorpicker",
      "heading" => __("Bar custom text color", "Nimva"),
      "param_name" => "customtextcolor",
      "description" => __("Select custom text color for bars.", "Nimva"),
      "dependency" => Array('element' => "bgcolor", 'value' => array('custom'))
    ),
	
	//$add_css_animation,

    array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  )
) );

/**
 * Pie chart
 */
vc_map( array(
    "name" => __("Pie chart", "Nimva"),
    "base" => "vc_pie",
    "class" => "",
    "icon" => "icon-wpb-vc_pie",
    "category" => __('Content', 'Nimva'),
	"description" => __('Animated pie chart', 'Nimva'),
    "params" => array(
		array(
			"type" => "range",
			"heading" => __( "Percent", "Nimva" ),
			"param_name" => "value",
			"description" => __('Input graph value here. Witihn a range 0-100.', 'Nimva'),
			"value" => "50",
			"min" => "0",
			"max" => "100",
			"step" => "1",
			"unit" => '%',
			"read" => 'readonly',
			"admin_label" => true			
		),
		array(
		  "type" => "colorpicker",
		  "heading" => __("Bar color", 'Nimva'),
		  "param_name" => "color",
		  "value" => '#f96e5b', //Default White color
		  "description" => __("Select the color of the circular bar", 'Nimva')
		),
		
		array(
		  "type" => "colorpicker",
		  "heading" => __("Track color", 'Nimva'),
		  "param_name" => "track_color",
		  "value" => '#ddd', //Default White color
		  "description" => __("Select the color of the track for the bar", 'Nimva')
		),
		
		array(
			"type" => "range",
			"heading" => __( "Bar Width", "Nimva" ),
			"param_name" => "bar_width",
			"description" => __('Select the width of the bar line.', 'Nimva'),
			"value" => "5",
			"min" => "1",
			"max" => "20",
			"step" => "1",
			"unit" => 'px',
			"admin_label" => true			
		),
		
		array(
		  "type" => "dropdown",
		  "heading" => __("Content inside Pie", "Nimva"),
		  "param_name" => "style",
		  "value" => array(__("Percent", "Nimva") => "percent", __("Custom Value Counter", "Nimva") => "custom_counter", __("Icon", "Nimva") => "icon", __("Custom Text", "Nimva") => "custom_text"),
		  "description" => __("Choose what will be displayed inside the Pie.", "Nimva"),
		  "admin_label" => true
		 
		),
		
		array(
            "type" => "textfield",
            "heading" => __("Custom Value", "Nimva"),
            "param_name" => "custom_value",
			"value" => "",
            "description" => __("Add an integer value to be displayed inside the pie. This value will also be used as a counter.", "Nimva"),
			"dependency" => Array('element' => 'style', 'value' => array('custom_counter'))
        ),
		
		array(
            "type" => "textfield",
            "heading" => __("Custom Symbol", "Nimva"),
            "param_name" => "symbol",
			"value" => "",
            "description" => __("Add a custom symbol to the Custom Counter.", "Nimva"),
			"dependency" => Array('element' => 'style', 'value' => array('custom_counter'))
        ),
		
		array(
		  "type" => "dropdown",
		  "heading" => __("Content Symbol Position", "Nimva"),
		  "param_name" => "symbol_position",
		  "value" => array(__("Before", "Nimva") => "before", __("After", "Nimva") => "after"),
		  "description" => __("Choose the position of the custom symbol.", "Nimva"),
		  "dependency" => Array('element' => 'style', 'value' => array('custom_counter'))		 
		),
		
		array(
		  "type" => "colorpicker",
		  "heading" => __("Content inside Pie - color", 'Nimva'),
		  "param_name" => "content_color",
		  "value" => '#777777', //Default White color
		  "description" => __("Select the color of the content that appears inside the circular bar", 'Nimva')
		),
		
		array(
			"type" => "range",
			"heading" => __( "Content inside Pie - font size", "Nimva" ),
			"param_name" => "content_size",
			"description" => __('Select the font size of the content that appears inside the circular bar.', 'Nimva'),
			"value" => "29",
			"min" => "10",
			"max" => "50",
			"step" => "1",
			"unit" => 'px'
		),
		
		array(
			"type" => "awesome_icons",
			"heading" => __( "Icon", "Nimva" ),
			"param_name" => "icon",
			"width" => 200,
			"value" => $awesome_icons_list,
			"encoding" => "false",
			"dependency" => Array('element' => 'style', 'value' => array('icon'))
		),
		
		array(
            "type" => "textfield",
            "heading" => __("Custom Text", "Nimva"),
            "param_name" => "custom_text_field",
			"value" => "",
            "description" => __("Add a custom text to be displayed inside the pie graph.", "Nimva"),
			"dependency" => Array('element' => 'style', 'value' => array('custom_text'))
        ),
		array(
            "type" => "textarea",
            "heading" => __("Short Description", "Nimva"),
            "param_name" => "description",
			"value" => "",
            "description" => __("Add a description bellow the pie chart.", "Nimva"),			
        ),
		
		array(
		  "type" => "colorpicker",
		  "heading" => __("Short description - color", 'Nimva'),
		  "param_name" => "desc_color",
		  "value" => '#777777', //Default White color
		  "description" => __("Select the color of the short description", 'Nimva')
		),
		
		array(
			"type" => "range",
			"heading" => __( "Short description - font size", "Nimva" ),
			"param_name" => "desc_size",
			"description" => __('Select the font size of the short description.', 'Nimva'),
			"value" => "16",
			"min" => "10",
			"max" => "40",
			"step" => "1",
			"unit" => 'px'
		),		
		
		/*
        array(
            "type" => "textfield",
            "heading" => __("Pie label value", "Nimva"),
            "param_name" => "label_value",
            "description" => __('Input integer value for label. If empty "Pie value" will be used.', 'Nimva'),
            "value" => ""
        ),
        array(
            "type" => "textfield",
            "heading" => __("Units", "Nimva"),
            "param_name" => "units",
            "description" => __("Enter measurement units (if needed) Eg. %, px, points, etc. Graph value and unit will be appended to the graph title.", "Nimva")
        ),
		*/
		
		$add_css_animation,
		
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "Nimva"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
        ),

    )
) );



/* Support for 3rd Party plugins
---------------------------------------------------------- */
// Contact form 7 plugin
include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); // Require plugin.php to use is_plugin_active() below
if (is_plugin_active('contact-form-7/wp-contact-form-7.php')) {
  global $wpdb;
  $cf7 = $wpdb->get_results( 
  	"
  	SELECT ID, post_title 
  	FROM $wpdb->posts
  	WHERE post_type = 'wpcf7_contact_form' 
  	"
  );
  $contact_forms = array();
  if ($cf7) {
    foreach ( $cf7 as $cform ) {
      $contact_forms[$cform->post_title] = $cform->ID;
    }
  } else {
    $contact_forms["No contact forms found"] = 0;
  }
  vc_map( array(
    "base" => "contact-form-7",
    "name" => __("Contact Form 7", "Nimva"),
    "icon" => "icon-wpb-contactform7",
    "category" => __('Content', 'Nimva'),
	"description" => __('Place Contact Form7', 'Nimva'),
    "params" => array(
      array(
        "type" => "textfield",
        "heading" => __("Form title", "Nimva"),
        "param_name" => "title",
        "admin_label" => true,
        "description" => __("What text use as form title. Leave blank if no title is needed.", "Nimva")
      ),
      array(
        "type" => "dropdown",
        "heading" => __("Select contact form", "Nimva"),
        "param_name" => "id",
        "value" => $contact_forms,
        "description" => __("Choose previously created contact form from the drop down list.", "Nimva")
      )
    )
  ) );
} // if contact form7 plugin active

if (is_plugin_active('LayerSlider/layerslider.php')) {
  global $wpdb;
  $ls = $wpdb->get_results( 
  	"
  	SELECT id, name, date_c
  	FROM ".$wpdb->prefix."layerslider
  	WHERE flag_hidden = '0' AND flag_deleted = '0'
  	ORDER BY date_c ASC LIMIT 100
  	"
  );
  $layer_sliders = array();
  if ($ls) {
    foreach ( $ls as $slider ) {
      $layer_sliders[$slider->name] = $slider->id;
    }
  } else {
    $layer_sliders["No sliders found"] = 0;
  }
  vc_map( array(
    "base" => "layerslider_vc",
    "name" => __("Layer Slider", "Nimva"),
    "icon" => "icon-wpb-layerslider",
    "category" => __('Content', 'Nimva'),
	"description" => __('Place LayerSlider', 'Nimva'),
    "params" => array(
      array(
        "type" => "textfield",
        "heading" => __("Widget title", "Nimva"),
        "param_name" => "title",
        "description" => __("What text use as a widget title. Leave blank if no title is needed.", "Nimva")
      ),
      array(
        "type" => "dropdown",
        "heading" => __("LayerSlider ID", "Nimva"),
        "param_name" => "id",
        "admin_label" => true,
        "value" => $layer_sliders,
        "description" => __("Select your LayerSlider.", "Nimva")
      ),
      array(
        "type" => "textfield",
        "heading" => __("Extra class name", "Nimva"),
        "param_name" => "el_class",
        "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
      )
    )
  ) );
} // if layer slider plugin active

if (is_plugin_active('revslider/revslider.php')) {
  global $wpdb;
  $rs = $wpdb->get_results( 
  	"
  	SELECT id, title, alias
  	FROM ".$wpdb->prefix."revslider_sliders
  	ORDER BY id ASC LIMIT 100
  	"
  );
  $revsliders = array();
  if ($rs) {
    foreach ( $rs as $slider ) {
      $revsliders[$slider->title] = $slider->alias;
    }
  } else {
    $revsliders["No sliders found"] = 0;
  }
  vc_map( array(
    "base" => "rev_slider_vc",
    "name" => __("Revolution Slider", "Nimva"),
    "icon" => "icon-wpb-revslider",
    "category" => __('Content', 'Nimva'),
	"description" => __('Place Revolution slider', 'Nimva'),
    "params"=> array(
      array(
        "type" => "textfield",
        "heading" => __("Widget title", "Nimva"),
        "param_name" => "title",
        "description" => __("What text use as a widget title. Leave blank if no title is needed.", "Nimva")
      ),
      array(
        "type" => "dropdown",
        "heading" => __("Revolution Slider", "Nimva"),
        "param_name" => "alias",
        "admin_label" => true,
        "value" => $revsliders,
        "description" => __("Select your Revolution Slider.", "Nimva")
      ),
      array(
        "type" => "textfield",
        "heading" => __("Extra class name", "Nimva"),
        "param_name" => "el_class",
        "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
      )
    )
  ) );
} // if revslider plugin active

if (is_plugin_active('gravityforms/gravityforms.php')) {
  $gravity_forms_array[__("No Gravity forms found.", "Nimva")] = '';
  if ( class_exists('RGFormsModel') ) {
    $gravity_forms = RGFormsModel::get_forms(1, "title");
    if ($gravity_forms) {
      $gravity_forms_array = array(__("Select a form to display.", "Nimva") => '');
      foreach ( $gravity_forms as $gravity_form ) {
        $gravity_forms_array[$gravity_form->title] = $gravity_form->id;
      }
    }
  }
  vc_map( array(
    "name" => __("Gravity Form", "Nimva"),
    "base" => "gravityform",
    "icon" => "icon-wpb-vc_gravityform",
    "category" => __("Content", "Nimva"),
	"description" => __('Place Gravity form', 'Nimva'),
    "params" => array(
      array(
        "type" => "dropdown",
        "heading" => __("Form", "Nimva"),
        "param_name" => "id",
        "value" => $gravity_forms_array,
        "description" => __("Select a form to add it to your post or page.", "Nimva"),
        "admin_label" => true
      ),
      array(
        "type" => "dropdown",
        "heading" => __("Display Form Title", "Nimva"),
        "param_name" => "title",
        "value" => array( __("No", "Nimva") => 'false', __("Yes", "Nimva") => 'true' ),
        "description" => __("Would you like to display the forms title?", "Nimva"),
        "dependency" => Array('element' => "id", 'not_empty' => true)
      ),
      array(
        "type" => "dropdown",
        "heading" => __("Display Form Description", "Nimva"),
        "param_name" => "description",
        "value" => array( __("No", "Nimva") => 'false', __("Yes", "Nimva") => 'true' ),
        "description" => __("Would you like to display the forms description?", "Nimva"),
        "dependency" => Array('element' => "id", 'not_empty' => true)
      ),
      array(
        "type" => "dropdown",
        "heading" => __("Enable AJAX?", "Nimva"),
        "param_name" => "ajax",
        "value" => array( __("No", "Nimva") => 'false', __("Yes", "Nimva") => 'true' ),
        "description" => __("Enable AJAX submission?", "Nimva"),
        "dependency" => Array('element' => "id", 'not_empty' => true)
      ),
      array(
        "type" => "textfield",
        "heading" => __("Tab Index", "Nimva"),
        "param_name" => "tabindex",
        "description" => __("(Optional) Specify the starting tab index for the fields of this form. Leave blank if you're not sure what this is.", "Nimva"),
        "dependency" => Array('element' => "id", 'not_empty' => true)
      )
    )
  ) );
} // if gravityforms active

/* Flickr
---------------------------------------------------------- */
vc_map( array(
  "base" => "vc_flickr",
  "name" => __("Flickr Widget", "Nimva"),
  "icon" => "icon-wpb-flickr",
  "category" => __('Content', 'Nimva'),
  "description" => __('Image feed from your flickr account', 'Nimva'),
  "params"	=> array(
    array(
      "type" => "textfield",
      "heading" => __("Widget title", "Nimva"),
      "param_name" => "title",
      "description" => __("What text use as a widget title. Leave blank if no title is needed.", "Nimva")
    ),
    array(
      "type" => "textfield",
      "heading" => __("Flickr ID", "Nimva"),
      "param_name" => "flickr_id",
      'admin_label' => true,
      "description" => sprintf(__('To find your flickID visit %s.', "Nimva"), '<a href="http://idgettr.com/" target="_blank">idGettr</a>')
    ),
    array(
      "type" => "dropdown",
      "heading" => __("Number of photos", "Nimva"),
      "param_name" => "count",
      "value" => array(9, 8, 7, 6, 5, 4, 3, 2, 1),
      "description" => __("Number of photos.", "Nimva")
    ),
    array(
      "type" => "dropdown",
      "heading" => __("Type", "Nimva"),
      "param_name" => "type",
      "value" => array(__("User", "Nimva") => "user", __("Group", "Nimva") => "group"),
      "description" => __("Photo stream type.", "Nimva")
    ),
    array(
      "type" => "dropdown",
      "heading" => __("Display", "Nimva"),
      "param_name" => "display",
      "value" => array(__("Latest", "Nimva") => "latest", __("Random", "Nimva") => "random"),
      "description" => __("Photo order.", "Nimva")
    ),
	
	$add_css_animation,
	
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  )
) );

/* Facebook like button
---------------------------------------------------------- */
vc_map( array(
  "name" => __("Facebook Like", "Nimva"),
  "base" => "vc_facebook",
  "icon" => "icon-wpb-balloon-facebook-left",
  "category" => __('Social', 'Nimva'),
  "description" => __('Facebook like button', 'Nimva'),
  "params" => array(
    array(
      "type" => "dropdown",
      "heading" => __("Button type", "Nimva"),
      "param_name" => "type",
      "admin_label" => true,
      "value" => array(__("Standard", "Nimva") => "standard", __("Button count", "Nimva") => "button_count", __("Box count", "Nimva") => "box_count"),
      "description" => __("Select button type.", "Nimva")
    )
  )
) );

/* Tweetmeme button
---------------------------------------------------------- */
vc_map( array(
  "name" => __("Tweetmeme Button", "Nimva"),
  "base" => "vc_tweetmeme",
  "icon" => "icon-wpb-tweetme",
  "category" => __('Social', 'Nimva'),
  "description" => __('Share on twitter button', 'Nimva'),
  "params" => array(
    array(
      "type" => "dropdown",
      "heading" => __("Button type", "Nimva"),
      "param_name" => "type",
      "admin_label" => true,
      "value" => array(__("Horizontal", "Nimva") => "horizontal", __("Vertical", "Nimva") => "vertical", __("None", "Nimva") => "none"),
      "description" => __("Select button type.", "Nimva")
    )
  )
) );

/* Google+ button
---------------------------------------------------------- */
vc_map( array(
  "name" => __("Google+ Button", "Nimva"),
  "base" => "vc_googleplus",
  "icon" => "icon-wpb-application-plus",
  "category" => __('Social', 'Nimva'),
  "description" => __('Recommend on Google', 'Nimva'),
  "params" => array(
    array(
      "type" => "dropdown",
      "heading" => __("Button size", "Nimva"),
      "param_name" => "type",
      "admin_label" => true,
      "value" => array(__("Standard", "Nimva") => "", __("Small", "Nimva") => "small", __("Medium", "Nimva") => "medium", __("Tall", "Nimva") => "tall"),
      "description" => __("Select button size.", "Nimva")
    ),
    array(
      "type" => "dropdown",
      "heading" => __("Annotation", "Nimva"),
      "param_name" => "annotation",
      "admin_label" => true,
      "value" => array(__("Inline", "Nimva") => "inline", __("Bubble", "Nimva") => "", __("None", "Nimva") => "none"),
      "description" => __("Select annotation type.", "Nimva")
    )
  )
) );

/* Google+ button
---------------------------------------------------------- */
vc_map( array(
  "name" => __("Pinterest Button", "Nimva"),
  "base" => "vc_pinterest",
  "icon" => "icon-wpb-pinterest",
  "category" => __('Social', 'Nimva'),
  "description" => __('Add a Pinterest button', 'Nimva'),
  "params"	=> array(
    array(
      "type" => "dropdown",
      "heading" => __("Button layout", "Nimva"),
      "param_name" => "type",
      "admin_label" => true,
      "value" => array(__("Horizontal", "Nimva") => "", __("Vertical", "Nimva") => "vertical", __("No count", "Nimva") => "none"),
      "description" => __("Select button layout.", "Nimva")
    )
  )
) );


/* Widgetised sidebar
---------------------------------------------------------- */

vc_map( array(
  "name" => __("Widgetised Sidebar", "Nimva"),
  "base" => "vc_widget_sidebar",
  "class" => "wpb_widget_sidebar_widget",
  "icon" => "icon-wpb-layout_sidebar",
  "category" => __('Structure', 'Nimva'),
  "description" => __('Place widgetised sidebar', 'Nimva'),
  "params" => array(
    array(
      "type" => "textfield",
      "heading" => __("Widget title", "Nimva"),
      "param_name" => "title",
      "description" => __("What text use as a widget title. Leave blank if no title is needed.", "Nimva")
    ),
    array(
      "type" => "widgetised_sidebars",
      "heading" => __("Sidebar", "Nimva"),
      "param_name" => "sidebar_id",
      "description" => __("Select which widget area output.", "Nimva")
    ),
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  )
) );

/* WordPress default Widgets (Appearance->Widgets)
---------------------------------------------------------- */


vc_map( array(
  "name" => 'WP ' . __("Search"),
  "base" => "vc_wp_search",
  "icon" => "icon-wpb-wp",
  "category" => __("WordPress Widgets", "Nimva"),
  "class" => "wpb_vc_wp_widget",
  "params" => array(
    array(
      "type" => "textfield",
      "heading" => __("Widget title", "Nimva"),
      "param_name" => "title",
      "description" => __("What text use as a widget title. Leave blank to use default widget title.", "Nimva")
    ),
	
	$add_css_animation,
	
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  )
) );

vc_map( array(
  "name" => 'WP ' . __("Meta"),
  "base" => "vc_wp_meta",
  "icon" => "icon-wpb-wp",
  "category" => __("WordPress Widgets", "Nimva"),
  "class" => "wpb_vc_wp_widget",
  "params" => array(
    array(
      "type" => "textfield",
      "heading" => __("Widget title", "Nimva"),
      "param_name" => "title",
      "description" => __("What text use as a widget title. Leave blank to use default widget title.", "Nimva")
    ),
	
	$add_css_animation,
	
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  )
) );

vc_map( array(
  "name" => 'WP ' . __("Recent Comments"),
  "base" => "vc_wp_recentcomments",
  "icon" => "icon-wpb-wp",
  "category" => __("WordPress Widgets", "Nimva"),
  "class" => "wpb_vc_wp_widget",
  "params" => array(
    array(
      "type" => "textfield",
      "heading" => __("Widget title", "Nimva"),
      "param_name" => "title",
      "description" => __("What text use as a widget title. Leave blank to use default widget title.", "Nimva")
    ),
    array(
      "type" => "textfield",
      "heading" => __("Number of comments to show", "Nimva"),
      "param_name" => "number",
      "admin_label" => true
    ),
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  )
) );

vc_map( array(
  "name" => 'WP ' . __("Calendar"),
  "base" => "vc_wp_calendar",
  "icon" => "icon-wpb-wp",
  "category" => __("WordPress Widgets", "Nimva"),
  "class" => "wpb_vc_wp_widget",
  "params" => array(
    array(
      "type" => "textfield",
      "heading" => __("Widget title", "Nimva"),
      "param_name" => "title",
      "description" => __("What text use as a widget title. Leave blank to use default widget title.", "Nimva")
    ),
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  )
) );

vc_map( array(
  "name" => 'WP ' . __("Pages"),
  "base" => "vc_wp_pages",
  "icon" => "icon-wpb-wp",
  "category" => __("WordPress Widgets", "Nimva"),
  "class" => "wpb_vc_wp_widget",
  "params" => array(
    array(
      "type" => "textfield",
      "heading" => __("Widget title", "Nimva"),
      "param_name" => "title",
      "description" => __("What text use as a widget title. Leave blank to use default widget title.", "Nimva")
    ),
    array(
      "type" => "dropdown",
      "heading" => __("Sort by", "Nimva"),
      "param_name" => "sortby",
      "value" => array(__("Page title", "Nimva") => "post_title", __("Page order", "Nimva") => "menu_order", __("Page ID", "Nimva") => "ID"),
      "admin_label" => true
    ),
    array(
      "type" => "textfield",
      "heading" => __("Exclude", "Nimva"),
      "param_name" => "exclude",
      "description" => __("Page IDs, separated by commas.", "Nimva"),
      "admin_label" => true
    ),
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  )
) );

$tag_taxonomies = array();
foreach ( get_taxonomies() as $taxonomy ) :
  $tax = get_taxonomy($taxonomy);
	if ( !$tax->show_tagcloud || empty($tax->labels->name) )
  	continue;
	$tag_taxonomies[$tax->labels->name] = esc_attr($taxonomy);
endforeach;
vc_map( array(
  "name" => 'WP ' . __("Tag Cloud"),
  "base" => "vc_wp_tagcloud",
  "icon" => "icon-wpb-wp",
  "category" => __("WordPress Widgets", "Nimva"),
  "class" => "wpb_vc_wp_widget",
  "params" => array(
    array(
      "type" => "textfield",
      "heading" => __("Widget title", "Nimva"),
      "param_name" => "title",
      "description" => __("What text use as a widget title. Leave blank to use default widget title.", "Nimva")
    ),
    array(
      "type" => "dropdown",
      "heading" => __("Taxonomy", "Nimva"),
      "param_name" => "taxonomy",
      "value" => $tag_taxonomies,
      "admin_label" => true
    ),
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  )
) );

$custom_menus = array();
$menus = get_terms( 'nav_menu', array( 'hide_empty' => false ) );
if ( $menus ) {
  foreach ( $menus as $single_menu ) {
    $custom_menus[$single_menu->name] = $single_menu->term_id;
  }
  vc_map( array(
    "name" => 'WP ' . __("Custom Menu"),
    "base" => "vc_wp_custommenu",
    "icon" => "icon-wpb-wp",
    "category" => __("WordPress Widgets", "Nimva"),
    "class" => "wpb_vc_wp_widget",
    "params" => array(
      array(
        "type" => "textfield",
        "heading" => __("Widget title", "Nimva"),
        "param_name" => "title",
        "description" => __("What text use as a widget title. Leave blank to use default widget title.", "Nimva")
      ),
      array(
        "type" => "dropdown",
        "heading" => __("Menu", "Nimva"),
        "param_name" => "nav_menu",
        "value" => $custom_menus,
        "description" => __("Select menu", "Nimva"),
        "admin_label" => true
      ),
      array(
        "type" => "textfield",
        "heading" => __("Extra class name", "Nimva"),
        "param_name" => "el_class",
        "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
      )
    )
  ) );
}

vc_map( array(
  "name" => 'WP ' . __("Text"),
  "base" => "vc_wp_text",
  "icon" => "icon-wpb-wp",
  "category" => __("WordPress Widgets", "Nimva"),
  "class" => "wpb_vc_wp_widget",
  "params" => array(
    array(
      "type" => "textfield",
      "heading" => __("Widget title", "Nimva"),
      "param_name" => "title",
      "description" => __("What text use as a widget title. Leave blank to use default widget title.", "Nimva")
    ),
    array(
      "type" => "textarea",
      "heading" => __("Text", "Nimva"),
      "param_name" => "text",
      "admin_label" => true
    ),
    /*array(
      "type" => "checkbox",
      "heading" => __("Automatically add paragraphs", "Nimva"),
      "param_name" => "filter"
    ),*/
	
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  )
) );


vc_map( array(
  "name" => 'WP ' . __("Recent Posts"),
  "base" => "vc_wp_posts",
  "icon" => "icon-wpb-wp",
  "category" => __("WordPress Widgets", "Nimva"),
  "class" => "wpb_vc_wp_widget",
  "params" => array(
    array(
      "type" => "textfield",
      "heading" => __("Widget title", "Nimva"),
      "param_name" => "title",
      "description" => __("What text use as a widget title. Leave blank to use default widget title.", "Nimva")
    ),
    array(
      "type" => "textfield",
      "heading" => __("Number of posts to show", "Nimva"),
      "param_name" => "number",
      "admin_label" => true
    ),
    array(
      "type" => "checkbox",
      "heading" => __("Display post date?", "Nimva"),
      "param_name" => "show_date",
      "value" => array(__("Display post date?") => true )
    ),
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  )
) );


$link_category = array(__("All Links", "Nimva") => "");
$link_cats = get_terms( 'link_category' );
foreach ( $link_cats as $link_cat ) {
  $link_category[$link_cat->name] = $link_cat->term_id;
}
vc_map( array(
  "name" => 'WP ' . __("Links"),
  "base" => "vc_wp_links",
  "icon" => "icon-wpb-wp",
  "category" => __("WordPress Widgets", "Nimva"),
  "class" => "wpb_vc_wp_widget",
  "params" => array(
    array(
      "type" => "dropdown",
      "heading" => __("Link Category", "Nimva"),
      "param_name" => "category",
      "value" => $link_category,
      "admin_label" => true
    ),
    array(
      "type" => "dropdown",
      "heading" => __("Sort by", "Nimva"),
      "param_name" => "orderby",
      "value" => array(__("Link title", "Nimva") => "name", __("Link rating", "Nimva") => "rating", __("Link ID", "Nimva") => "id", __("Random", "Nimva") => "rand")
    ),
    array(
      "type" => "checkbox",
      "heading" => __("Options", "Nimva"),
      "param_name" => "options",
      "value" => array(__("Show Link Image", "Nimva") => "images", __("Show Link Name", "Nimva") => "name", __("Show Link Description", "Nimva") => "description", __("Show Link Rating", "Nimva") => "rating")
    ),
    array(
      "type" => "textfield",
      "heading" => __("Number of links to show", "Nimva"),
      "param_name" => "limit"
    ),
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  )
) );


vc_map( array(
  "name" => 'WP ' . __("Categories"),
  "base" => "vc_wp_categories",
  "icon" => "icon-wpb-wp",
  "category" => __("WordPress Widgets", "Nimva"),
  "class" => "wpb_vc_wp_widget",
  "params" => array(
    array(
      "type" => "textfield",
      "heading" => __("Widget title", "Nimva"),
      "param_name" => "title",
      "description" => __("What text use as a widget title. Leave blank to use default widget title.", "Nimva")
    ),
    array(
      "type" => "checkbox",
      "heading" => __("Options", "Nimva"),
      "param_name" => "options",
      "value" => array(__("Display as dropdown", "Nimva") => "dropdown", __("Show post counts", "Nimva") => "count", __("Show hierarchy", "Nimva") => "hierarchical")
    ),
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  )
) );


vc_map( array(
  "name" => 'WP ' . __("Archives"),
  "base" => "vc_wp_archives",
  "icon" => "icon-wpb-wp",
  "category" => __("WordPress Widgets", "Nimva"),
  "class" => "wpb_vc_wp_widget",
  "params" => array(
    array(
      "type" => "textfield",
      "heading" => __("Widget title", "Nimva"),
      "param_name" => "title",
      "description" => __("What text use as a widget title. Leave blank to use default widget title.", "Nimva")
    ),
    array(
      "type" => "checkbox",
      "heading" => __("Options", "Nimva"),
      "param_name" => "options",
      "value" => array(__("Display as dropdown", "Nimva") => "dropdown", __("Show post counts", "Nimva") => "count")
    ),
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  )
) );

/* Custom Flickr Widget */

vc_map( array(
  "name" => __("Flickr Widget (Custom)", "Nimva"),
  "base" => "vc_wp_flickr_widget",
  "icon" => "icon-wpb-wp",
  "category" => __("WordPress Widgets", "Nimva"),
  "class" => "wpb_vc_wp_widget",
  "params" => array(
    array(
      "type" => "textfield",
      "heading" => __("Widget title", "Nimva"),
      "param_name" => "title",
      "description" => __("What text use as a widget title. Leave blank to use default widget title.", "Nimva")
    ),
	array(
      "type" => "textfield",
      "heading" => __("Screen name", "Nimva"),
      "param_name" => "screen_name",
	  "admin_label" => true,
      "description" => __("Inser the user name from flickr.", "Nimva")
    ),
    array(
      "type" => "dropdown",
      "heading" => __("Picture count", "Nimva"),
      "param_name" => "number",
	  "admin_label" => true,
	  "description" => __("Select how many pictures to show.", "Nimva"),
      "value" => array(1,2,3,4,5,6,7,8,9,10,11,12)
    ),
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  )
) );

vc_map( array(
  "name" => __("Recent Posts (Custom)", "Nimva"),
  "base" => "vc_wp_custom_posts",
  "icon" => "icon-wpb-wp",
  "category" => __("WordPress Widgets", "Nimva"),
  "class" => "wpb_vc_wp_widget",
  "params" => array(
    array(
      "type" => "textfield",
      "heading" => __("Widget title", "Nimva"),
      "param_name" => "title",
      "description" => __("What text to use as a widget title. Leave blank to use default widget title.", "Nimva")
    ),
    array(
      "type" => "dropdown",
      "heading" => __("Items count", "Nimva"),
      "param_name" => "number",
	  "admin_label" => true,
	  "description" => __("Select how many recent articles to show.", "Nimva"),
      "value" => array(1,2,3,4,5,6,7,8,9,10,11,12)
    ),
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  )
) );

vc_map( array(
  "name" => __("Recent Portfolio (Custom)", "Nimva"),
  "base" => "vc_wp_custom_portf",
  "icon" => "icon-wpb-wp",
  "category" => __("WordPress Widgets", "Nimva"),
  "class" => "wpb_vc_wp_widget",
  "params" => array(
    array(
      "type" => "textfield",
      "heading" => __("Widget title", "Nimva"),
      "param_name" => "title",
      "description" => __("What text to use as a widget title. Leave blank to use default widget title.", "Nimva")
    ),
    array(
      "type" => "dropdown",
      "heading" => __("Items count", "Nimva"),
      "param_name" => "number",
	  "admin_label" => true,
	  "description" => __("Select how many recent articles to show.", "Nimva"),
      "value" => array(1,2,3,4,5,6,7,8,9,10,11,12)
    ),
    array(
      "type" => "textfield",
      "heading" => __("Short Description", "Nimva"),
      "param_name" => "desc",
	  "admin_label" => true,
	  "value" => "",
      "description" => __("You can enter a short text to describe your portfolio.", "Nimva")
    ),	
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  )
) );

vc_map( array(
  "name" => __("Popular / Recent Posts Tabs (Custom)", "Nimva"),
  "base" => "vc_wp_rec_pop_tabs",
  "icon" => "icon-wpb-wp",
  "category" => __("WordPress Widgets", "Nimva"),
  "class" => "wpb_vc_wp_widget",
  "params" => array(
    array(
      "type" => "textfield",
      "heading" => __("Widget title", "Nimva"),
      "param_name" => "title",
      "description" => __("What text to use as a widget title. Leave blank to use default widget title.", "Nimva")
    ),
	
	array(
      "type" => "dropdown",
      "heading" => __("Show popular posts", "Nimva"),
      "param_name" => "show_popular_posts",
	  "admin_label" => true,
      "value" => array( __("Yes", "Nimva") => 'true', __("No", "Nimva") => 'false' )
    ),

    array(
      "type" => "dropdown",
      "heading" => __("Popular posts count", "Nimva"),
      "param_name" => "posts",
	  "description" => __("Select how many popular posts to show in tabs.", "Nimva"),
      "value" => array(1,2,3,4,5,6,7,8,9,10,11,12),
	  "dependency" => Array('element' => 'show_popular_posts', 'value' => array('true'))
    ),
	
	array(
      "type" => "dropdown",
      "heading" => __("Show recent posts", "Nimva"),
      "param_name" => "show_recent_posts",
	  "admin_label" => true,
      "value" => array( __("Yes", "Nimva") => 'true', __("No", "Nimva") => 'false' )
    ),
	
	array(
      "type" => "dropdown",
      "heading" => __("Recent posts count", "Nimva"),
      "param_name" => "recent",
	  "description" => __("Select how many recent posts to show in tabs.", "Nimva"),
      "value" => array(1,2,3,4,5,6,7,8,9,10,11,12),
	  "dependency" => Array('element' => 'show_recent_posts', 'value' => array('true'))
    ),
	
	array(
      "type" => "dropdown",
      "heading" => __("Show comments", "Nimva"),
      "param_name" => "show_comments",
	  "admin_label" => true,
      "value" => array( __("Yes", "Nimva") => 'true', __("No", "Nimva") => 'false' )
    ),
	
	array(
      "type" => "dropdown",
      "heading" => __("Comments count", "Nimva"),
      "param_name" => "comments",
	  "description" => __("Select how many comments to show in tabs.", "Nimva"),
      "value" => array(1,2,3,4,5,6,7,8,9,10,11,12),
	  "dependency" => Array('element' => 'show_comments', 'value' => array('true'))
    ),
	
	array(
      "type" => "dropdown",
      "heading" => __("Tabs Color", "Nimva"),
      "param_name" => "color",
	  "admin_label" => true,
	  "description" => __("Select the color of the tabs.", "Nimva"),
      "value" => $button_colors
    ),
	
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  )
) );

vc_map( array(
  "name" => __("Contact us (Custom)", "Nimva"),
  "base" => "vc_wp_contact_us",
  "icon" => "icon-wpb-wp",
  "category" => __("WordPress Widgets", "Nimva"),
  "class" => "wpb_vc_wp_widget",
  "params" => array(
    array(
      "type" => "textfield",
      "heading" => __("Widget title", "Nimva"),
      "param_name" => "title",
      "description" => __("What text to use as a widget title. Leave blank to use default widget title.", "Nimva")
    ),
	array(
      "type" => "textarea",
      "heading" => __("Intro Text", "Nimva"),
      "param_name" => "intro",
	  "admin_label" => true,
      "description" => __("Enter some intro text.", "Nimva")
    ),
    array(
      "type" => "textfield",
      "heading" => __("Address", "Nimva"),
      "param_name" => "address",
	  "admin_label" => true,
      "description" => __("Enter your contact address.", "Nimva")
    ),
	array(
      "type" => "textfield",
      "heading" => __("Phone", "Nimva"),
      "param_name" => "phone",
	  "admin_label" => true,
      "description" => __("Enter your contact phone.", "Nimva")
    ),

	array(
      "type" => "textfield",
      "heading" => __("Email", "Nimva"),
      "param_name" => "email",
	  "admin_label" => true,
      "description" => __("Enter your contact email.", "Nimva")
    ),
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  )
) );


vc_map( array(
  "name" => __("Social Links (Custom)", "Nimva"),
  "base" => "vc_wp_social_links",
  "icon" => "icon-wpb-wp",
  "category" => __("WordPress Widgets", "Nimva"),
  "class" => "wpb_vc_wp_widget",
  "params" => array(
    array(
      "type" => "textfield",
      "heading" => __("Widget title", "Nimva"),
      "param_name" => "title",
      "description" => __("What text to use as a widget title. Leave blank to use default widget title.", "Nimva")
    ),
    array(
      "type" => "textfield",
      "heading" => __("Facbook Link", "Nimva"),
      "param_name" => "fb_link",
	  "admin_label" => true,
      "description" => __("Enter your Facebook profile/page link.", "Nimva")
    ),
	array(
      "type" => "textfield",
      "heading" => __("Twitter Link", "Nimva"),
      "param_name" => "twitter_link",
	  "admin_label" => true,
      "description" => __("Enter your Twitter profile link.", "Nimva")
    ),	
	array(
      "type" => "textfield",
      "heading" => __("Google Link", "Nimva"),
      "param_name" => "google_link",
	  "admin_label" => true,
      "description" => __("Enter your Google+ profile link.", "Nimva")
    ),
	array(
      "type" => "textfield",
      "heading" => __("Instagram Link", "Nimva"),
      "param_name" => "instagram_link",
	  "admin_label" => true,
      "description" => __("Enter your Instagram profile link.", "Nimva")
    ),
	array(
      "type" => "textfield",
      "heading" => __("LinkedIn Link", "Nimva"),
      "param_name" => "linkedin_link",
	  "admin_label" => true,
      "description" => __("Enter your LinkedIn profile link.", "Nimva")
    ),
	
	array(
      "type" => "textfield",
      "heading" => __("Dribbble Link", "Nimva"),
      "param_name" => "dribbble_link",
	  "admin_label" => true,
      "description" => __("Enter your Dribbble profile link.", "Nimva")
    ),
	
	array(
      "type" => "textfield",
      "heading" => __("Pinterest Link", "Nimva"),
      "param_name" => "pinterest_link",
	  "admin_label" => true,
      "description" => __("Enter your Pinterest profile link.", "Nimva")
    ),
	
	array(
      "type" => "textfield",
      "heading" => __("Flickr Link", "Nimva"),
      "param_name" => "flickr_link",
	  "admin_label" => true,
      "description" => __("Enter your Flickr profile link.", "Nimva")
    ),
	array(
      "type" => "textfield",
      "heading" => __("Tumblr Link", "Nimva"),
      "param_name" => "tumblr_link",
	  "admin_label" => true,
      "description" => __("Enter your Tumblr profile link.", "Nimva")
    ),
	array(
      "type" => "textfield",
      "heading" => __("Behance Link", "Nimva"),
      "param_name" => "behance_link",
	  "admin_label" => true,
      "description" => __("Enter your Behance profile link.", "Nimva")
    ),
	
	array(
      "type" => "textfield",
      "heading" => __("Skype Link", "Nimva"),
      "param_name" => "skype_link",
	  "admin_label" => true,
      "description" => __("Enter your Skype profile link.", "Nimva")
    ),
	array(
      "type" => "textfield",
      "heading" => __("Vimeo Link", "Nimva"),
      "param_name" => "vimeo_link",
	  "admin_label" => true,
      "description" => __("Enter your Vimeo profile link.", "Nimva")
    ),

	array(
      "type" => "textfield",
      "heading" => __("YouTube Link", "Nimva"),
      "param_name" => "youtube_link",
	  "admin_label" => true,
      "description" => __("Enter your YouTube profile link.", "Nimva")
    ),

    array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  )
) );


vc_map( array(
  "name" => __("Twitter Feed (Custom)", "Nimva"),
  "base" => "vc_wp_twitter_feed",
  "icon" => "icon-wpb-wp",
  "category" => __("WordPress Widgets", "Nimva"),
  "class" => "wpb_vc_wp_widget",
  "params" => array(
    array(
      "type" => "textfield",
      "heading" => __("Widget title", "Nimva"),
      "param_name" => "title",
      "description" => __("What text to use as a widget title. Leave blank to use default widget title.", "Nimva")
    ),
    array(
      "type" => "textfield",
      "heading" => __("Consumer Key", "Nimva"),
      "param_name" => "consumer_key",
      "description" => __("Enter your twitter api consumer key.", "Nimva")
    ),
	array(
      "type" => "textfield",
      "heading" => __("Consumer Secret", "Nimva"),
      "param_name" => "consumer_secret",
      "description" => __("Enter your twitter api consumer secret.", "Nimva")
    ),
	array(
      "type" => "textfield",
      "heading" => __("Access Token", "Nimva"),
      "param_name" => "access_token",
      "description" => __("Enter your twitter api access token.", "Nimva")
    ),
	array(
      "type" => "textfield",
      "heading" => __("Access Token Secret", "Nimva"),
      "param_name" => "access_token_secret",
      "description" => __("Enter your twitter api access token secret.", "Nimva")
    ),
	array(
      "type" => "textfield",
      "heading" => __("Twitter ID", "Nimva"),
      "param_name" => "twitter_id",
	  "admin_label" => true,
      "description" => __("Enter your twitter id.", "Nimva")
    ),
	array(
      "type" => "dropdown",
      "heading" => __("Number of tweets", "Nimva"),
      "param_name" => "count",
	  "description" => __("Select how many tweets to show.", "Nimva"),
      "value" => array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15),
	  "admin_label" => true
    ),
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  )
) );

/*
//BETA: Not ready for use on live website
vc_map( array(
  "name" => 'WP ' . __("RSS"),
  "base" => "vc_wp_rss",
  "icon" => "icon-wpb-wp",
  "category" => __("WordPress Widgets", "Nimva"),
  "class" => "wpb_vc_wp_widget",
  "params" => array(
    array(
      "type" => "textfield",
      "heading" => __("Widget title", "Nimva"),
      "param_name" => "title",
      "description" => __("What text use as a widget title. Leave blank to use default widget title.", "Nimva")
    ),
    array(
      "type" => "textfield",
      "heading" => __("RSS feed URL", "Nimva"),
      "param_name" => "url",
      "description" => __("Enter the RSS feed URL.", "Nimva"),
      "admin_label" => true
    ),
    array(
      "type" => "dropdown",
      "heading" => __("Items", "Nimva"),
      "param_name" => "items",
      "value" => array(__("10 - Default", "Nimva") => '', 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20),
      "description" => __("How many items would you like to display?", "Nimva"),
      "admin_label" => true
    ),
    array(
      "type" => "checkbox",
      "heading" => __("Options", "Nimva"),
      "param_name" => "options",
      "value" => array(__("Display item content?", "Nimva") => "show_summary", __("Display item author if available?", "Nimva") => "show_author", __("Display item date?", "Nimva") => "show_date")
    ),
    array(
      "type" => "textfield",
      "heading" => __("Extra class name", "Nimva"),
      "param_name" => "el_class",
      "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
    )
  )
) );

vc_map( array(
    "name" => __("Items", "Nimva"),
    "base" => "vc_items",
    "as_parent" => array('only' => 'vc_item'),
    "content_element" => true,
    "params" => array(
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "Nimva"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
        )
    ),
    "js_view" => 'VcColumnView'
) );
vc_map( array(
    "name" => __("Item", "Nimva"),
    "base" => "vc_item",
    "content_element" => true,
    "as_child" => array('only' => 'vc_items'),
    "params" => array(
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "Nimva"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
        )
    )
) );

class WPBakeryShortCode_VC_Items extends WPBakeryShortCode_VC_Column {

}
/**
 * New teaser grid !!
 */
/*vc_map( array(
    "name" => __("Posts grid", "Nimva"),
    "base" => "vc_posts_grid",
    "is_container" => true,
    "params" => array(
        array(
            "type" => "textfield",
            "heading" => __("Widget title", "Nimva"),
            "param_name" => "title",
            "description" => __("What text use as a widget title. Leave blank if no title is needed.", "Nimva")
        ),
        array(
            "type" => "dropdown",
            "heading" => __("Columns count", "Nimva"),
            "param_name" => "grid_columns_count",
            "value" => array(4, 3, 2, 1),
            "admin_label" => true,
            "description" => __("Select columns count.", "Nimva")
        ),
        array(
            "type" => "dropdown",
            "heading" => __("Content", "Nimva"),
            "param_name" => "grid_content",
            "value" => array(__("Teaser (Excerpt)", "Nimva") => "teaser", __("Full Content", "Nimva") => "content"),
            "description" => __("Teaser layout template.", "Nimva")
        ),
        array(
            "type" => "teaser_template",
            "heading" => __("Layout", "Nimva"),
            "param_name" => "grid_layout",
            "description" => __("Teaser layout.", "Nimva")
        ),
        array(
            "type" => "dropdown",
            "heading" => __("Link", "Nimva"),
            "param_name" => "grid_link",
            "value" => array(__("Link to post", "Nimva") => "link_post", __("Link to bigger image", "Nimva") => "link_image", __("Thumbnail to bigger image, title to post", "Nimva") => "link_image_post", __("No link", "Nimva") => "link_no"),
            "description" => __("Link type.", "Nimva")
        ),
        array(
            "type" => "dropdown",
            "heading" => __("Link target", "Nimva"),
            "param_name" => "grid_link_target",
            "value" => $target_arr,
            "dependency" => Array('element' => "grid_link", 'value' => array('link_post', 'link_image_post'))
        ),
        array(
            "type" => "dropdown",
            "heading" => __("Teaser grid layout", "Nimva"),
            "param_name" => "grid_template",
            "value" => array(__("Grid", "Nimva") => "grid", __("Grid with filter", "Nimva") => "filtered_grid", __("Carousel", "Nimva") => "carousel"),
            "description" => __("Teaser layout template.", "Nimva")
        ),
        array(
            "type" => "dropdown",
            "heading" => __("Layout mode", "Nimva"),
            "param_name" => "grid_layout_mode",
            "value" => array(__("Fit rows", "Nimva") => "fitRows", __('Masonry', "Nimva") => 'masonry'),
            "dependency" => Array('element' => 'grid_template', 'value' => array('filtered_grid', 'grid')),
            "description" => __("Teaser layout template.", "Nimva")
        ),
        array(
            "type" => "textfield",
            "heading" => __("Thumbnail size", "Nimva"),
            "param_name" => "grid_thumb_size",
            "description" => __('Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height).', "Nimva")
        ),
        array(
            "type" => "textfield",
            "heading" => __("Extra class name", "Nimva"),
            "param_name" => "el_class",
            "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "Nimva")
        ),
        array(
            "type" => "teaser_template",
            "heading" => __("Layout", "Nimva"),
            "param_name" => "grid_layout",
            "description" => __("Teaser layout.", "Nimva")
        ),
        array(
            "type" => "loop",
            "heading" => __("Loop", "Nimva"),
            "param_name" => "loop",
            'settings' => array(
                'size' => array('hidden' => false, 'value' => 90),
                'order_by' => array('value' => 'date'),
            ),
            "description" => __("Create super mega query.", "Nimva")
        ),
        array(
            "type" => "vc_link",
            "heading" => __("Link", "Nimva"),
            "param_name" => "link",
            "description" => __("This adds a link selection box", "Nimva")
        )
    )
    // 'html_template' => dirname(__DIR__).'/composer/shortcodes_templates/vc_posts_grid.php'
) );*/
/*
VcTeaserTemplates::getInstance('vc_posts_grid', 'teaser_template');

WPBMap::layout(array('id'=>'column_12', 'title'=>'1/2'));
WPBMap::layout(array('id'=>'column_12-12', 'title'=>'1/2 + 1/2'));
WPBMap::layout(array('id'=>'column_13', 'title'=>'1/3'));
WPBMap::layout(array('id'=>'column_13-13-13', 'title'=>'1/3 + 1/3 + 1/3'));
WPBMap::layout(array('id'=>'column_13-23', 'title'=>'1/3 + 2/3'));
WPBMap::layout(array('id'=>'column_14', 'title'=>'1/4'));
WPBMap::layout(array('id'=>'column_14-14-14-14', 'title'=>'1/4 + 1/4 + 1/4 + 1/4'));
WPBMap::layout(array('id'=>'column_16', 'title'=>'1/6'));
WPBMap::layout(array('id'=>'column_11', 'title'=>'1/1'));
*/

