<?php // Template Name: Our Clients ?>
<?php get_header(); ?>
<?php 
if(get_post_meta($post->ID, 'pyre_slider_layer', true) != 0) { 
?>  
    <div id="slider-output">
    	<?php echo do_shortcode('[layerslider id="'.get_post_meta($post->ID, 'pyre_slider_layer', true).'"]'); ?>
    </div>
<?php 
}
?>
<?php get_header(); ?>               
<div class="container clearfix">
    <div class="client-page-wrapper">
        <div class="row">
        <?php
            wp_reset_query();
            $args = array(
                'orderby' => 'date',
                'order' => 'ASC',
                'posts_per_page' => 100,
                'post_type' => 'clients'
            );
            //
            $wp_query = new WP_Query( $args );
            while ( $wp_query->have_posts() ) {
                $wp_query->the_post();
                $post_id = get_the_ID();
                ?>

                <div class="col-sm-4 col-md-3">
                    <div class="client-item">
                        <div class="overlay">
                            <div class="title"><?php echo the_title(); ?>
                            </div>
                            <?php if (get_post_meta($post_id, 'pyre_client_url', true) && ICL_LANGUAGE_CODE == 'en') { ?>
                                <a class="link" target="_blank" href="<?php echo get_post_meta($post_id, 'pyre_client_url', true);  ?>">Client Website</a>
                            <?php } ?>
	                        <?php if (get_post_meta($post_id, 'pyre_client_url', true) && ICL_LANGUAGE_CODE == 'he') { ?>
                                <a class="link" target="_blank" href="<?php echo get_post_meta($post_id, 'pyre_client_url', true);  ?>">קישור לאתר הלקוח</a>
	                        <?php } ?>
                        </div>
                    <?php echo get_the_post_thumbnail( $post_id, 'large' ); ?>
                    </div>
                </div>



        <?php } ?>
        </div><!-- END: ROW -->
    </div>
</div>
<?php get_footer(); ?>