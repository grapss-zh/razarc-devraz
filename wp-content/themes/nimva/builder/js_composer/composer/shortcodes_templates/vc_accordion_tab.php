<?php
$output = $title = '';

extract(shortcode_atts(array(
	'title' => __("Section", "js_composer"),
	'icon' => ''
), $atts));

if( !empty($icon) ) {
	$icon = '<i class="' . $icon . '"></i>';
	$extra = 'render-icon';
}

$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'wpb_accordion_section group', $this->settings['base']);
$output .= "\n\t\t\t" . '<div class="'.$css_class.'">';
    $output .= "\n\t\t\t\t" . '<div class="acctitle ' . $extra . ' ui-accordion-header ">' . $icon . $title . '</div>';
    $output .= "\n\t\t\t\t" . '<div class="acc_content clearfix">';
        $output .= ($content=='' || $content==' ') ? __("Empty section. Edit page to add content here.", "js_composer") : "\n\t\t\t\t" . do_shortcode($content);
    $output .= "\n\t\t\t\t" . '</div>';
$output .= "\n\t\t\t" . '</div> ' . $this->endBlockComment('.wpb_accordion_section') . "\n";

echo $output;