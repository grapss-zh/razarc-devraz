<?php
$output = $category = $orderby = $options = $el_class = '';
extract( shortcode_atts( array(
    'title' => '',
	'show_popular_posts' => '',
	'posts' => '',
	'show_recent_posts' => '',
	'comments' => '',
	'show_comments' => '',
	'recent' => '',
    'color' => '3',
    'el_class' => ''
), $atts ) );

$el_class = $this->getExtraClass($el_class);

$output = '<div class="vc_widget '.$el_class.' clearfix">';
$type = 'Pyre_Tabs_Widget';
$args = array( 'before_title'=>'<div class="title-outer"><h3>', 'after_title'=>'</h3></div>');

ob_start();
the_widget( $type, $atts, $args );
$output .= ob_get_clean();

$output .= '</div>' . $this->endBlockComment('vc_wp_links') . "\n";

echo $output;