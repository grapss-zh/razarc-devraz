<?php
$output = $title = $el_class = $nav_menu = '';
extract( shortcode_atts( array(
    'title' => '',
    'nav_menu' => '',
    'el_class' => ''
), $atts ) );
$el_class = $this->getExtraClass($el_class);

$output = '<div class="vc_widget '.$el_class.' clearfix">';
$type = 'WP_Nav_Menu_Widget';
$args = array( 'before_title'=>'<div class="title-outer"><h3>', 'after_title'=>'</h3></div>');

ob_start();
the_widget( $type, $atts, $args );
$output .= ob_get_clean();

$output .= '</div>' . $this->endBlockComment('vc_wp_custommenu') . "\n";

echo $output;