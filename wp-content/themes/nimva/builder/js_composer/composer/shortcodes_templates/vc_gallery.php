<?php
$output = $title = $type = $onclick = $custom_links = $img_size = $custom_links_target = $images = $el_class = $interval = '';
extract(shortcode_atts(array(
    'title' => '',
    'type' => 'flexslider_fade',
    'onclick' => 'link_image',
    'custom_links' => '',
    'custom_links_target' => '',
    'img_size' => 'thumbnail',
	'slider_width' => '',
    'images' => '',
    'el_class' => '',
	'css_animation' => '',
	'image_border' => '',
	'border_color' => '',
    'interval' => '5',
), $atts));
$gal_images = '';
$link_start = '';
$link_end = '';
$el_start = '';
$el_end = '';
$slides_wrap_start = '';
$slides_wrap_end = '';

$el_class = $this->getExtraClass($el_class);

if ( $type == 'flexslider_fade' || $type == 'flexslider_slide') {
	
	if( $type == 'flexslider_fade' ) $fx = 'fade';
	else $fx = 'slide';
	
	if( $interval != 0 ) {
		$slideshow = 'data-slideshow="true"' . ' data-pause="' . ( $interval * 1000 ) . '" data-pause-hover="true"';
	}
	else $slideshow = '';
	
    $el_start = '<div class="slide">';
    $el_end = '</div>';
    $slides_wrap_start = '<div class="fslider" data-animate="'.$fx.'" data-direction-nav="true" '.$slideshow.'><div class="flexslider"><div class="slider-wrap">';
    $slides_wrap_end = '</div></div></div>';
} else if ( $type == 'image_grid' ) {
    wp_enqueue_script( 'isotope' );

    $el_start = '<li class="isotope-item">';
    $el_end = '</li>';
    $slides_wrap_start = '<ul class="wpb_image_grid_ul">';
    $slides_wrap_end = '</ul>';
}

if ( $onclick == 'link_image' ) {
    wp_enqueue_script( 'prettyphoto' );
    wp_enqueue_style( 'prettyphoto' );
}

//add user width to slider

if ( $type == 'flexslider_fade' || $type == 'flexslider_slide' ) {
	if( $slider_width ) {
		$custom_width = 'style = "max-width:' . $slider_width .'"';
	}
}

$flex_fx = '';
$style = '';

if ( $type == 'flexslider_fade') {
    $type = ' wpb_flexslider flexslider_fade flexslider';
	
} else if ( $type == 'flexslider_slide' ) {	
    $type = ' wpb_flexslider flexslider_slide flexslider';
	
} else if ( $type == 'image_grid' ) {
    $type = ' wpb_image_grid';
	
	if( $image_border > 0 ) {
		$style = 'style="border: ' . $image_border . 'px solid ' . $border_color . ';"';
	}
}

if ( $images == '' ) $images = '-1,-2,-3';

$pretty_rel_random = ' rel="prettyPhoto[rel-'.rand().']"'; //rel-'.rand();

if ( $onclick == 'custom_link' ) { $custom_links = explode( ',', $custom_links); }
$images = explode( ',', $images);
$i = -1;

foreach ( $images as $attach_id ) {
    $i++;
    if ($attach_id > 0) {
        $post_thumbnail = wpb_getImageBySize(array( 'attach_id' => $attach_id, 'thumb_size' => $img_size, 'style' => $style ));
    }
    else {
        $post_thumbnail = array();
        $post_thumbnail['thumbnail'] = '<img src="'.get_template_directory_uri().'/builder/js_composer/assets/vc/no_image.png'.'" />';
        $post_thumbnail['p_img_large'][0] = get_template_directory_uri().'/builder/js_composer/assets/vc/no_image.png';
    }

    $thumbnail = $post_thumbnail['thumbnail'];
    $p_img_large = $post_thumbnail['p_img_large'];
    $link_start = $link_end = '';

    if ( $onclick == 'link_image' ) {
        $link_start = '<a class="prettyphoto" href="'.$p_img_large[0].'"'.$pretty_rel_random.'>';
        $link_end = '</a>';
    }
    else if ( $onclick == 'custom_link' && isset( $custom_links[$i] ) && $custom_links[$i] != '' ) {
        $link_start = '<a href="'.$custom_links[$i].'"' . (!empty($custom_links_target) ? ' target="'.$custom_links_target.'"' : '') . '>';
        $link_end = '</a>';
    }
    $gal_images .= $el_start . $link_start . $thumbnail . $link_end . $el_end;
}
$css_class =  apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'wpb_gallery wpb_content_element'.$el_class.' clearfix', $this->settings['base']);
$css_class .= $this->getCSSAnimation($css_animation);
$output .= "\n\t".'<div class="'.$css_class.'" '.$custom_width.'>';
$output .= "\n\t\t".'<div class="wpb_wrapper">';
$output .= wpb_widget_title(array('title' => $title, 'extraclass' => 'wpb_gallery_heading'));
$output .= '<div class="wpb_gallery_slides'.$type.'">'.$slides_wrap_start.$gal_images.$slides_wrap_end.'</div>';
$output .= "\n\t\t".'</div> '.$this->endBlockComment('.wpb_wrapper');
$output .= "\n\t".'</div> '.$this->endBlockComment('.wpb_gallery');

echo $output;