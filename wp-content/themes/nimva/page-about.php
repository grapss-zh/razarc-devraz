<?php // Template Name: About Us ?>
<?php get_header(); ?>

<?php $lang = ICL_LANGUAGE_CODE; ?>


<div class="container clearfix">

    <div class="row members-wrap">
        <?php
        $args = array(
            'post_type' => 'employees',
            'showposts' => 2,
            'offset' => 0
            //'nopaging' => true
        );
        ?>

        <?php $members = new WP_Query($args); ?>
        <?php $countMembers = 1; ?>
        <?php while($members->have_posts()): $members->the_post(); ?>

            <?php $postId = get_the_ID(); ?>

        <?php if ($lang == 'he' ) { ?>
	        <div class="col-sm-6 col-md-4 col-md-pull-2">
        <?php } else { ?>
	        <div class="col-sm-6 col-md-4 col-md-push-2">
        <?php } ?>

                <?php if ($countMembers <= 2 ) { ?>
                    <div class="team-member team-member-first-line">
                <?php } else  { ?>
                    <div class="team-member">
                <?php } ?>

                    <?php if(has_post_thumbnail()) { ?>
                        <?php
                        $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
                        if(get_post_meta($postId, 'pyre_employee_position', true) != '') {
                            $position = get_post_meta($postId, 'pyre_employee_position', true);
                        }
                        ?>

                        <div class="team-image">
                                <img src="<?php echo $image[0]; ?>" alt="<?php echo get_the_title(); ?> - <?php echo $position ?>" title="<?php echo get_the_title(); ?> - <?php echo $position; ?>">
                            <?php if(get_post_meta(get_the_ID(), 'pyre_employee_position', true) != '') { ?>
                                <span><?php echo $position; ?></span>
                            <?php } ?>
                        </div>
                    <?php } ?>

                    <div class="team-desc">
                        <h4><?php echo get_the_title(); ?></h4>
                        <?php if(get_post_meta($postId, 'pyre_employee_bio', true) != '') { ?>
                            <p><?php echo get_post_meta($postId, 'pyre_employee_bio', true); ?></p>
                        <?php } ?>
                    </div>

                    <div class="team-phone-mail">
                        <?php if( get_post_meta($postId, 'pyre_employee_phone', true) != ''): ?>
                            <p class="emp_phone"><span>phone: </span><?php echo get_post_meta(get_the_ID(), 'pyre_employee_phone', true); ?></p>
                        <?php endif; ?>

                        <?php if( get_post_meta($postId, 'pyre_employee_email', true) != ''): ?>
                            <p class="emp_email"><span>email: </span><?php echo get_post_meta(get_the_ID(), 'pyre_employee_email', true); ?></p>
                        <?php endif; ?>
                    </div>



                    <ul class="team-skills">

                        <?php if(get_post_meta($postId, 'pyre_employee_facebook', true) != '') { ?>
                        <li><a href="<?php echo get_post_meta(get_the_ID(), 'pyre_employee_facebook', true); ?>"  class="ntip" alt="Facebook" original-title="Facebook"><i class="fa-facebook"></i></a></li>
                            <?php } ?>

                            <?php if(get_post_meta($postId, 'pyre_employee_twitter', true) != '') { ?>
                        <li><a href="<?php echo get_post_meta($postId, 'pyre_employee_twitter', true); ?>"  class="twitter ntip" alt="Twiter" original-title="Twitter"><i class="fa-twitter"></i></a></li>
                            <?php } ?>

                            <?php if(get_post_meta($postId, 'pyre_employee_google', true) != '') { ?>
                        <li><a href="<?php echo get_post_meta($postId, 'pyre_employee_google', true); ?>  class="gplus ntip" alt="Google" original-title="Google"><i class="fa-google-plus"></i></a></li>
                            <?php } ?>

                            <?php if(get_post_meta($postId, 'pyre_employee_linkedin', true) != '') { ?>
                        <li><a href="<?php echo get_post_meta($postId, 'pyre_employee_linkedin', true); ?>'"  class="linkedin ntip" alt="LinkedIn" original-title="LinkedIn"><i class="fa-linkedin"></i></a></li>
                            <?php } ?>

                            <?php if(get_post_meta($postId, 'pyre_employee_vimeo', true) != '') { ?>
                        <li><a href="<?php echo get_post_meta($postId, 'pyre_employee_vimeo', true); ?> class="vimeo ntip" alt="Vimeo" original-title="Vimeo"><i class="fa-vimeo-square"></i></a></li>
                            <?php } ?>

                            <?php if(get_post_meta($postId, 'pyre_employee_flickr', true) != '') { ?>
                        <li><a href="<?php echo get_post_meta($postId, 'pyre_employee_flickr', true); ?>  class="flickr ntip" alt="Flickr" original-title="Flickr"><i class="fa-flickr"></i></a></li>
                            <?php } ?>

                    </ul>
                </div><!-- END: Team member-->
            </div>

        <?php $countMembers++; ?>
        <?php endwhile; ?>
    </div>


<?php
$args = array(
	'post_type' => 'employees',
	'showposts' => 10,
	'offset' => 2
	//'nopaging' => true
);
?>

<?php $members = new WP_Query($args); ?>

<?php while($members->have_posts()): $members->the_post(); ?>

<?php $postId = get_the_ID(); ?>

    <div class="col-sm-6 col-md-4">
	<?php if ($countMembers <= 2 ) { ?>
    <div class="team-member team-member-first-line">
		<?php } else  { ?>
        <div class="team-member">
			<?php } ?>

			<?php if(has_post_thumbnail()) { ?>
				<?php
				$image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
				if(get_post_meta($postId, 'pyre_employee_position', true) != '') {
					$position = get_post_meta($postId, 'pyre_employee_position', true);
				}
				?>

                <div class="team-image">
                    <img src="<?php echo $image[0]; ?>" alt="<?php echo get_the_title(); ?> - <?php echo $position ?>" title="<?php echo get_the_title(); ?> - <?php echo $position; ?>">
					<?php if(get_post_meta(get_the_ID(), 'pyre_employee_position', true) != '') { ?>
                        <span><?php echo $position; ?></span>
					<?php } ?>
                </div>
			<?php } ?>

            <div class="team-desc">
                <h4><?php echo get_the_title(); ?></h4>
				<?php if(get_post_meta($postId, 'pyre_employee_bio', true) != '') { ?>
                    <p><?php echo get_post_meta($postId, 'pyre_employee_bio', true); ?></p>
				<?php } ?>
            </div>

            <div class="team-phone-mail">
				<?php if( get_post_meta($postId, 'pyre_employee_phone', true) != ''): ?>
                    <p class="emp_phone"><span>phone: </span><?php echo get_post_meta(get_the_ID(), 'pyre_employee_phone', true); ?></p>
				<?php endif; ?>

				<?php if( get_post_meta($postId, 'pyre_employee_email', true) != ''): ?>
                    <p class="emp_email"><span>email: </span><?php echo get_post_meta(get_the_ID(), 'pyre_employee_email', true); ?></p>
				<?php endif; ?>
            </div>



            <ul class="team-skills">

				<?php if(get_post_meta($postId, 'pyre_employee_facebook', true) != '') { ?>
                    <li><a href="<?php echo get_post_meta(get_the_ID(), 'pyre_employee_facebook', true); ?>"  class="ntip" alt="Facebook" original-title="Facebook"><i class="fa-facebook"></i></a></li>
				<?php } ?>

				<?php if(get_post_meta($postId, 'pyre_employee_twitter', true) != '') { ?>
                    <li><a href="<?php echo get_post_meta($postId, 'pyre_employee_twitter', true); ?>"  class="twitter ntip" alt="Twiter" original-title="Twitter"><i class="fa-twitter"></i></a></li>
				<?php } ?>

				<?php if(get_post_meta($postId, 'pyre_employee_google', true) != '') { ?>
                    <li><a href="<?php echo get_post_meta($postId, 'pyre_employee_google', true); ?>  class="gplus ntip" alt="Google" original-title="Google"><i class="fa-google-plus"></i></a></li>
				<?php } ?>

				<?php if(get_post_meta($postId, 'pyre_employee_linkedin', true) != '') { ?>
                    <li><a href="<?php echo get_post_meta($postId, 'pyre_employee_linkedin', true); ?>'"  class="linkedin ntip" alt="LinkedIn" original-title="LinkedIn"><i class="fa-linkedin"></i></a></li>
				<?php } ?>

				<?php if(get_post_meta($postId, 'pyre_employee_vimeo', true) != '') { ?>
                    <li><a href="<?php echo get_post_meta($postId, 'pyre_employee_vimeo', true); ?> class="vimeo ntip" alt="Vimeo" original-title="Vimeo"><i class="fa-vimeo-square"></i></a></li>
				<?php } ?>

				<?php if(get_post_meta($postId, 'pyre_employee_flickr', true) != '') { ?>
                    <li><a href="<?php echo get_post_meta($postId, 'pyre_employee_flickr', true); ?>  class="flickr ntip" alt="Flickr" original-title="Flickr"><i class="fa-flickr"></i></a></li>
				<?php } ?>

            </ul>
        </div><!-- END: Team member-->
    </div>
	<?php $countMembers++; ?>
<?php endwhile; ?>


        <?php wp_reset_query(); ?>

    </div>

        <!--    Slick slider -->


	    <?php
	    $images = get_field('office_gallery');
	    if( $images ) { ?>

            <div class="slick-office-wrap" style="direction: ltr">
                <div id="slick-office">
				    <?php foreach( $images as $image ) { ?>
                        <div class="item">
                            <a href="<?php echo $image['url']; ?>" data-rel="lightcase:office">
                                <img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>">
                            </a>
                        </div>
				    <?php } ?>
                </div>
            </div>
            <hr class="about-sep-slider">
	    <?php } ?>



</div>
<?php get_footer(); ?>