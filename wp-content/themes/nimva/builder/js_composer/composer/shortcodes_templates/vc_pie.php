<?php
$title = $el_class = $value = $label_value= $units = '';

extract(shortcode_atts(array(	
	'value' => '50',
	'color' => '',
	'track_color' => '#ddd',
	'bar_width' => '5',
	'units' => '',	
	'label_value' => '',
	'style' => '',
	'custom_value' => '',
	'symbol' => '',
	'symbol_position' => '',
	'content_color' => '',
	'content_size' => '',
	'desc_color' => '',
	'desc_size' => '',
	'icon' => 'icon-cogs',
	'custom_text_field' => '',
	'description' => '',
	'css_animation' => '',
	'el_class' => '',
), $atts));

$css_animation = ($css_animation != '') ? 'wpb_animate_when_almost_visible wpb_'.$css_animation : '';

wp_enqueue_script('vc_pie');

$icon = ( $style == 'icon') ? 'data-pie-icon="'.$icon.'"' : '';

$custom_value = ( $style == 'custom_counter') ? 'data-custom-value="'.$custom_value.'"' : '';

$symbol = ( $style == 'custom_counter') ? 'data-symbol="'.$symbol.'"' : '';

$symbol_position = ( $style == 'custom_counter') ? 'data-symbol-pos="'.$symbol_position.'"' : '';

$text = ( $style == 'custom_text') ? 'data-pie-text="'.$custom_text_field.'"' : '';

$content_color = ($content_color != '#777777') ? 'color: ' . $content_color . ';' : '' ;
$content_size = ($content_size != '29') ? 'font-size: ' . $content_size . 'px;' : '' ;

$desc_color = ($desc_color != '#777777') ? 'color: ' . $desc_color . ';' : '' ;
$desc_size = ($desc_size != '29') ? 'font-size: ' . $desc_size . 'px;' : '' ;

$el_class = $this->getExtraClass( $el_class );
$css_class =  apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'vc_pie_chart wpb_content_element'.$el_class, $this->settings['base']);
$output = "\n\t".'<div class= "' . $css_class . ' ' . $css_animation . '" data-pie-type="'.$style.'" '. $custom_value . ' ' . $symbol . ' ' . $symbol_position . ' ' .$icon.' ' .$text. ' data-pie-value="'.$value.'" data-bar-width="'.$bar_width.'" data-pie-label-value="'.$label_value.'" data-pie-units="'.$units.'" data-pie-color="'.$color.'">';
    $output .= "\n\t\t".'<div class="wpb_wrapper">';
        $output .= "\n\t\t\t".'<div class="vc_pie_wrapper">';
            $output .= "\n\t\t\t".'<span class="vc_pie_chart_back" style="border: '.$bar_width.'px solid '.$track_color.'"></span>';
            $output .= "\n\t\t\t".'<span class="vc_pie_chart_value" style="' . $content_color . $content_size . '"></span>';
            $output .= "\n\t\t\t".'<canvas width="101" height="101"></canvas>';
			if( !empty($description) ) {
				$output .= "\n\t\t\t".'<div class="pie_description" style="' . $desc_color . $desc_size . '">'.$description.'</div>';
			}
            $output .= "\n\t\t\t".'</div>';
    $output .= "\n\t\t".'</div>'.$this->endBlockComment('.wpb_wrapper');
    $output .= "\n\t".'</div>'.$this->endBlockComment('.wpb_pie_chart')."\n";

echo $output;