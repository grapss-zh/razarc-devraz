<?php
/**
 * Show messages
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! $messages ) return;
?>

<?php foreach ( $messages as $message ) : ?>
	<div class="alert alert-success woocommerce_message"><strong><?php _e("Success!", "Nimva"); ?></strong><p class="nobottommargin"><?php echo wp_kses_post( $message ); ?></p></div>
<?php endforeach; ?>
