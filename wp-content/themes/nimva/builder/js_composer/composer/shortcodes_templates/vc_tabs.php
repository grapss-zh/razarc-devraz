<?php
$output = $color = $el_class = '';
extract(shortcode_atts(array(    
    'el_class' => ''
), $atts));



wp_enqueue_script('jquery-ui-tabs');

$el_class = $this->getExtraClass($el_class);

$element = 'wpb_tabs';

// Extract tab titles
preg_match_all( '/vc_tab title="([^\"]+)"(\stab_id\=\"([^\"]+)\"){0,1}/i', $content, $matches, PREG_OFFSET_CAPTURE );
$tab_titles = array();


if ( isset($matches[0]) ) { $tab_titles = $matches[0]; }
$tabs_nav = '';
$tabs_nav .= '<ul class="tabs wpb_tabs_nav2 ">';
if ( preg_match_all( "/(.?)\[(vc_tab)\b(.*?)(?:(\/))?\]/s", $content, $matches ) ) {
        for ( $i = 0; $i < count( $matches[ 0 ] ); $i++ ) {
            $matches[ 3 ][ $i ] = shortcode_parse_atts( $matches[ 3 ][ $i ] );
        }
        for ( $i = 0; $i < count( $matches[ 0 ] ); $i++ ) {
            $icon = isset($matches[ 3 ][ $i ][ 'icon' ]) ? $matches[ 3 ][ $i ][ 'icon' ] : '';
            if($icon == '') {
                $tabs_nav .= '<li><a href="#tab-'.$matches[ 3 ][ $i ][ 'tab_id' ].'" data-href="#tab-'.$matches[ 3 ][ $i ][ 'tab_id' ].'">' . $matches[ 3 ][ $i ][ 'title' ] . '</a></li>';
            } else {
                $tabs_nav .= '<li><a href="#tab-'.$matches[ 3 ][ $i ][ 'tab_id' ].'" data-href="#tab-'.$matches[ 3 ][ $i ][ 'tab_id' ].'"><i class="' . $icon . '"></i>' . $matches[ 3 ][ $i ][ 'title' ] . '</a></li>';
            }
    }
}
$tabs_nav .= '</ul>'."\n";

/*
$tabs_nav = '';
$tabs_nav .= '<ul class="tabs wpb_tabs_nav2">';
if ( preg_match_all( "/(.?)\[(vc_tab)\b(.*?)(?:(\/))?\]/s", $content, $matches ) ) {
        for ( $i = 0; $i < count( $matches[ 0 ] ); $i++ ) {
            $matches[ 3 ][ $i ] = shortcode_parse_atts( $matches[ 3 ][ $i ] );
        }
        for ( $i = 0; $i < count( $matches[ 0 ] ); $i++ ) {
            $icon = isset($matches[ 3 ][ $i ][ 'icon' ]) ? $matches[ 3 ][ $i ][ 'icon' ] : '';
            if($icon == '') {
                $tabs_nav .= '<li><a href="#tab-'.$matches[ 3 ][ $i ][ 'tab_id' ].'" data-href="#tab-'.$matches[ 3 ][ $i ][ 'tab_id' ].'">' . $matches[ 3 ][ $i ][ 'title' ] . '</a></li>';
            } else {
                $tabs_nav .= '<li><a href="#tab-'. $matches[ 3 ][ $i ][ 'tab_id' ] .'" data-href="#tab-'.$matches[ 3 ][ $i ][ 'tab_id' ].'"><i class="' . $icon . '"></i>' . $matches[ 3 ][ $i ][ 'title' ] . '</a></li>';
            }
    }
}

$tabs_nav .= '</ul>'."\n";

*/

$css_class =  apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, trim($element.' wpb_content_element '.$el_class), $this->settings['base']);

$output .= "\n\t".'<div class="tab_widget '.$css_class.'">';
$output .= "\n\t\t".'<div class="wpb_wrapper wpb_tour_tabs_wrapper ui-tabs vc_clearfix">';
$output .= "\n\t\t\t".$tabs_nav;
$output .= "\n\t\t\t".'<div class="tab_container">'.do_shortcode($content).'</div>';

$output .= "\n\t\t".'</div> '.$this->endBlockComment('.wpb_wrapper');
$output .= "\n\t".'</div> '.$this->endBlockComment($element);

/*
$css_class =  apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, trim($element.' wpb_content_element '.$el_class), $this->settings['base']);

$output .= "\n\t".'<div class="'.$css_class.'" data-interval="'.$interval.'">';
$output .= "\n\t\t".'<div class="wpb_wrapper wpb_tour_tabs_wrapper ui-tabs vc_clearfix">';

$output .= "\n\t\t\t".$tabs_nav;
$output .= "\n\t\t\t".wpb_js_remove_wpautop($content);

$output .= "\n\t\t".'</div> '.$this->endBlockComment('.wpb_wrapper');
$output .= "\n\t".'</div> '.$this->endBlockComment($element);
*/
echo $output;