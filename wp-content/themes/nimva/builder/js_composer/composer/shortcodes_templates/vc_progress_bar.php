<?php
$output = $title = $values = $units = $bgcolor = $custombgcolor = $options = $el_class = '';
extract( shortcode_atts( array(
    'title' => '',
    'values' => '',
    'units' => '',
    'bgcolor' => 'bar_grey',
    'custombgcolor' => '',
	'customtextcolor' => '',
    'style' => '',
    'el_class' => ''
), $atts ) );
wp_enqueue_script( 'waypoints' );

$el_class = $this->getExtraClass($el_class);

if( $style == 'strippes' ) {
	$style = 'progress-striped'; 
}
elseif ($style == 'animated_strippes') { 
	$style = 'progress-striped active'; 
}
else { 
	$style = ''; 
}

$custombgcolor = !empty($custombgcolor) ? 'background-color: '. $custombgcolor.'; ' : '';
$customtextcolor = !empty($customtextcolor) ? 'color: '. $customtextcolor.'; ' : '';

if ($bgcolor=="custom" && $custombgcolor!='') { $custombgcolor = ' style="background-color: '.$custombgcolor.';"'; $bgcolor=""; }
if ($bgcolor!="") $bgcolor = " ".$bgcolor;

$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'vc_progress_bar wpb_content_element'.$el_class, $this->settings['base']);
$output = '<div class="'.$css_class.'">';
$output .= wpb_widget_title(array('title' => $title, 'extraclass' => 'wpb_progress_bar_heading'));

$graph_lines = explode(",", $values);
$max_value = 0.0;
$graph_lines_data = array();
foreach ($graph_lines as $line) {
    $new_line = array();
    $color_index = 2;
    $data = explode("|", $line);
    $new_line['value'] = isset($data[0]) ? $data[0] : 0;
    $new_line['percentage_value'] = isset($data[1]) && preg_match('/^\d{1,2}\%$/', $data[1]) ? (float)str_replace('%', '', $data[1]) : false;
    if($new_line['percentage_value']!=false) {
        $color_index+=1;
        $new_line['label'] = isset($data[2]) ? $data[2] : '';
    } else {
        $new_line['label'] = isset($data[1]) ? $data[1] : '';
    }

    if($new_line['percentage_value']===false && $max_value < (float)$new_line['value']) {
        $max_value = $new_line['value'];
    }

    $graph_lines_data[] = $new_line;
}
foreach($graph_lines_data as $line) {
    $output .= '<div class="progress ' . $style . '">';	
    if($line['percentage_value']!==false) {
        $percentage_value = $line['percentage_value'];
    } elseif($max_value > 100.00) {
        $percentage_value = (float)$line['value'] > 0 && $max_value > 100.00 ? round((float)$line['value']/$max_value*100, 4) : 0;
    } else {
        $percentage_value = $line['value'];
    }
		$output .= '<div class="bar" data-value="'.$line['value'].'" style="'.$custombgcolor.$customtextcolor.'">';
			$output .= '<div class="describe">'.$line['label'].'</div>';
			$output .= '<div class="percentage">'.$line['value'].$units.'</div>';
		$output .= '</div>';	
    $output .= '</div>';
}

$output .= '</div>';

echo $output . $this->endBlockComment('progress_bar') . "\n";