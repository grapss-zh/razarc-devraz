<?php
$output = $color = $size = $icon = $target = $href = $el_class = $title = $position = '';
extract(shortcode_atts(array(
	'style' => 'minimal',
    'color' => 'green',
    'size' => 'small',
    'align' => '',
    'target' => '_self',
    'href' => '',
    'el_class' => '',
	'css_animation' => '',
    'title' => __('Text on the button', "js_composer"),
	'icon' => '',
    'position' => '',
	'button_inverse' => 'false'
), $atts));
$a_class = '';

$css_animation = ($css_animation != '') ? 'wpb_animate_when_almost_visible wpb_'.$css_animation : '';

if ( $el_class != '' ) {
    $tmp_class = explode(" ", strtolower($el_class));
    $tmp_class = str_replace(".", "", $tmp_class);
    if ( in_array("prettyphoto", $tmp_class) ) {
        wp_enqueue_script( 'prettyphoto' );
        wp_enqueue_style( 'prettyphoto' );
        $a_class .= ' prettyphoto';
        $el_class = str_ireplace("prettyphoto", "", $el_class);
    }
}

$align = !empty($align) ? ('style="text-align: ' . $align . ';"') : '';

$el_class = $this->getExtraClass($el_class);

$icon = !empty($icon) ? ( '<i class="fa '.$icon.'"></i>' ) : '';
$inverse = ($button_inverse == 'true') ? (' inverse ' ) : '';

if(!empty($align)):
	$output = '<div class="' . $el_class . ' ' . $css_animation .'" '.$align.'>';
endif;
		if( $style == 'minimal' ) :
			$output .= '<a href="'.$href.'" class="simple-button ' . $color . ' ' . $size . $inverse . ' ' . $el_class . '" target="'.$target.'">'. $icon . '<span>' . do_shortcode($title). '</span></a>';
		else:
			$output .= '<a href="'.$href.'" class="simple-button-3d ' . $color . ' ' . $size . $inverse . ' ' . $el_class . '" target="'.$target.'">'. $icon . '<span>' . do_shortcode($title). '</span></a>';
		endif;
if(!empty($align)):		
	$output .= '</div>';	
endif;	

echo $output . $this->endBlockComment('button') . "\n";
